#!/bin/sh
sample="LP130Gills"

# Trimming according to khmer protocols
export PYTHONPATH=$PYTHONPATH:/data/projects_3/diehl/holland/software/lib/python2.7/site-packages/
cd /data/processing/diehl/stephen/trim

java -jar /data/projects_3/diehl/holland/software/Trimmomatic-0.30/trimmomatic-0.30.jar PE -threads 24 /data/projects_3/boehm/holland/raw_data/131126_SN7001180_0080_BC37W0ACXX/renamed/${sample}_R1.fastq.gz /data/projects_3/boehm/holland/raw_data/131126_SN7001180_0080_BC37W0ACXX/renamed/${sample}_R2.fastq.gz s1_pe s1_se s2_pe s2_se ILLUMINACLIP:/data/projects_3/diehl/holland/software/Trimmomatic-0.30/adapters/TruSeq3-PE.fa:2:30:10

# Quality filter according to khmer protocols
/data/projects_3/diehl/holland/software/khmer/scripts/interleave-reads.py s1_pe s2_pe | /package/fastx_toolkit/fastq_quality_filter -v -Q33 -q 30 -p 50  | /package/fastx_toolkit/fastq_quality_trimmer -Q33 -t 15 > ${sample}.pe.qc.fq
cat s1_se s2_se | sed -e 's% %/%' -e 's/:N:.*//' | /package/fastx_toolkit/fastq_quality_filter -v -Q33 -q 30 -p 50 | /package/fastx_toolkit/fastq_quality_trimmer -Q33 -t 15 > ${sample}.se.qc.fq
rm -f s1_pe s1_se s2_pe s2_se

/data/projects_3/diehl/holland/software/khmer/scripts/extract-paired-reads.py ${sample}.pe.qc.fq
cat ${sample}.pe.qc.fq.se >> ${sample}.se.qc.fq
rm -f ${sample}.pe.qc.fq ${sample}.pe.qc.fq.se

/data/projects_3/diehl/holland/software/khmer/scripts/split-paired-reads.py ${sample}.pe.qc.fq.pe
/data/projects_3/diehl/holland/software/khmer/scripts/split-paired-reads.py ${sample}.se.qc.fq
rm -rf ${sample}.pe.qc.fq.pe ${sample}.se.qc.fq

cat ${sample}.pe.qc.fq.1 >> ${sample}.pe.qc.fq.pe.1
cat ${sample}.pe.qc.fq.2 >> ${sample}.pe.qc.fq.pe.2
mv ${sample}.pe.qc.fq.pe.1 ${sample}_qc_R1.fq
mv ${sample}.pe.qc.fq.pe.2 ${sample}_qc_R2.fq

rm -f ${sample}.pe.qc.fq.1 ${sample}.pe.qc.fq.2

# Trinity stranded assembly
export PATH=$PATH:/package/bowtie-0.12.9/

cd /data/processing/diehl/stephen/Trinity

#zcat /data/projects/diehl/holland/130307_SN7001180_0052_BC1V76ACXX/Project_StephenHollandThomasBoehm/${sample}_R2.fastq.gz | sed -e 's% %/%' -e 's/:N:.*//' > ${sample}17_R2.fastq &
#zcat /data/projects/diehl/holland/130307_SN7001180_0052_BC1V76ACXX/Project_StephenHollandThomasBoehm/${sample}_R1.fastq.gz | sed -e 's% %/%' -e 's/:N:.*//' > ${sample}17_R1.fastq &
#wait

/package/trinityrnaseq_r2013-02-25/util/normalize_by_kmer_coverage.pl --seqType fq --SS_lib_type RF --JM 100G --max_cov 30 --left ../trim/${sample}_qc_R1.fq --right ../trim/${sample}_qc_R2.fq --pairs_together --JELLY_CPU 12 --PARALLEL_STATS

/package/trinityrnaseq_r2013-02-25/Trinity.pl --seqType fq --SS_lib_type RF --JM 200G --left ${sample}_qc_R1.fastq.normalized_K25_C30_pctSD100.fq --right ${sample}_qc_R2.fastq.normalized_K25_C30_pctSD100.fq --output ${sample} --CPU 12 --bflyCPU 12 --inchworm_cpu 12

rm -rf normalized_reads/
gzip -9 ./trim/${sample}_qc_R1.fq
gzip -9 ./trim/${sample}_qc_R2.fq
gzip -9 ${sample}_qc_R1.fastq.normalized_K25_C30_pctSD100.fq
gzip -9 ${sample}_qc_R2.fastq.normalized_K25_C30_pctSD100.fq

cd /data/processing/diehl
cp Trinity/${sample}/Trinity.fasta /data/projects/diehl/holland/stranded_assembly/Trinity/${sample}_Trinity.fasta

# Adding ${sample} name to fasta ID
cd /data/processing/diehl
sed "s/^>/>${sample}_/" Trinity/${sample}/Trinity.fasta > ${sample}_Trinity.fa

# Clustering with cd-hit
~/tools/cd-hit-v4.6.1-2012-08-27/cd-hit-est -i ${sample}_Trinity.fa -o ${sample}_clstr.fa -c 0.98 -n 8 -T 12 -d 0 -M 1000


# ORF filtering
cd /data/processing/diehl/transdecoder
mkdir ${sample}
cd ${sample}

/package/trinityrnaseq_r2013-02-25/trinity-plugins/transdecoder/transcripts_to_best_scoring_ORFs.pl -t /data/processing/diehl/${sample}_clstr.fa -S --CPU 12
cat /data/processing/diehl/${sample}_clstr.fa | python /data/projects/diehl/holland/scripts/extract_orfs.py ${sample} > /data/projects/diehl/holland/stranded_assembly/${sample}_clstr_orfs.fa


# reformat fasta naming of transdecoder proteins
mkdir /data/projects/diehl/holland/stranded_assembly/transdecoder/${sample}
cp best_candidates.eclipsed_orfs_removed* /data/projects/diehl/holland/stranded_assembly/transdecoder/${sample}/
cd /data/projects/diehl/holland/stranded_assembly/transdecoder/${sample}

echo "\$0 ~ /^${sample}/ {print \">\"\$0}" '$0 !~' "/^${sample}/ {print \$0}" > awk.cmd
cat best_candidates.eclipsed_orfs_removed.pep | cut -f10 -d " " | awk -f awk.cmd > ${sample}_transdec_p.fa
cat best_candidates.eclipsed_orfs_removed.cds | cut -f10 -d " " | awk -f awk.cmd > ${sample}_transdec.fa
rm awk.cmd

# bed file without track line
grep -v "^track" best_candidates.eclipsed_orfs_removed.bed > ${sample}_orfs.bed

# cluster ORFs
cd /data/projects/diehl/holland/stranded_assembly/transdecoder
cd ${sample}
~/tools/cd-hit-v4.6.1-2012-08-27/cd-hit -i ${sample}_transdec_p.fa -o ${sample}_transdec_p_clstr.fa -c 0.98 -T 12 -d 0
cd ..
python ../../scripts/filter_short.py ${sample}/${sample}_transdec_p_clstr.fa > ${sample}_transdec_p_clstr_long.fa


# getorf
#/package/EMBOSS-6.5.7/bin/getorf -noreverse -minsize 300 -find 1 -sequence ../${sample}_clstr_orfs.fa -outseq ${sample}_getorf.fa

# InterProScan
# cd /data/projects_3/diehl/holland/software/lookup_service_42.0
#java -Xmx2000m -jar server-5RC6-jetty-console.war --port 8083 --headless
cd /data/projects_3/diehl/holland/software/lookup_service_5.3-46.0
java -Xmx2000m -jar server-5.3-46.0-jetty-console.war --port 8083 --headless

cd /data/processing/diehl/interproscan
sed 's/*//' /data/projects/diehl/holland/stranded_assembly/transdecoder/${sample}_transdec_p_clstr_long.fa > ${sample}_transdec.fa
#/data/projects/diehl/holland/software/interproscan-5-RC6/interproscan.sh -appl SignalP-EUK-4.0,TMHMM-2.0c -b ${sample}_transdec -iprlookup -goterms -i ${sample}_transdec.fa -t p --pathways
#/data/projects/diehl/holland/software/interproscan-5-RC6/interproscan.sh -appl Panther-7.2,PrositeProfiles-20.89,PfamA-26.0,PrositePatterns-20.89,PRINTS-42.0,SignalP-EUK-4.0,TMHMM-2.0c,SuperFamily-1.75 -b ${sample}_transdec -iprlookup -goterms -i ${sample}_transdec.fa -t p --pathways
/data/projects/diehl/holland/software/interproscan-5.3-46.0/interproscan.sh -appl Panther-8.1,PrositeProfiles-20.97,PfamA-27.0,PrositePatterns-20.97,PRINTS-42.0,SignalP-EUK-4.0,TMHMM-2.0c,SuperFamily-1.75 -b select1_rep_iprl -goterms -i representatives_iprl.fa -t p --pathways
cp ${sample}_transdec.tsv /data/projects/diehl/holland/stranded_assembly/interproscan/
cp ${sample}_transdec.gff3 /data/projects/diehl/holland/stranded_assembly/interproscan/


# Proteinortho
export PATH=$PATH:/package/ncbi-blast-2.2.28+/bin
/data/projects/diehl/holland/software/Proteinortho/proteinortho4.pl -p=blastp+ -a=12 -dir=. -o=${sample}_vs_${sample2} -verbose ../${sample}_getorf.fa ../${sample2}_getorf.fa


# Filter Proteinortho clusters that contain TMHMM annotated proteins
(for s in `cut -f1,4 interproscan/LP130Gills_transdec.tsv | grep "TMHMM" | cut -f1`; do grep $s proteinortho/Gills.tsv; done)| sort | uniq > LP130Gills_TM_proteinortho.tsv

cat LP13*Gills_TM_proteinortho.tsv | sort | uniq -c | awk '{OFS="\t"} $1 == 4 {print $2,$3,$4,$5,$6,$7,$8}' > Gills_TM_proteinortho.tsv


# BLAST
/package/ncbi-blast-2.2.28+/bin/blastp -query /data/projects/diehl/holland/stranded_assembly/transdecoder/${sample}_transdec_p_clstr_long.fa -db /data/projects/misc/database/blast/nr -evalue 5 -out ${sample}_vs_nr_blast.xml -outfmt 5 -max_target_seqs 3 -num_threads 12

# Blast2GO
~/tools/jre1.7.0_25/bin/javaws /data/projects/diehl/holland/software/blast2go2000.jnlp
