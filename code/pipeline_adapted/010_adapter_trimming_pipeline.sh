#!/bin/bash -x

# Author: Sarah Diehl

# This scripts does a de-novo transcriptome assembly with Trinity.
# Steps in detail:
# - trimming of adapter sequences with Trimmomatic
# - filtering low quality reads and removing low quality trailing bases with fastx_toolkit
# - digital normalisation with Trinity's normalize_by_kmer_coverage.pl (optional, suggested for > 200 mio reads)
# - (stranded) de-novo transcriptome assembly with Trinity
# - ORF prediction with Trinity's transdecoder
# - collapsing identical protein ORF sequences with cd-hit

# References:
# De novo transcript sequence reconstruction from RNA-seq using the Trinity platform for reference generation and analysis. Haas et. al, Nature Protocols 2013
# khmer "Eel Pond" protocol for mRNAseq assembly version 0.8.3 https://khmer-protocols.readthedocs.org/en/v0.8.4/mrnaseq/index.html


function usage
{
    echo "usage: adapter_trimming_pipeline.sh -n sample [[[-d directory ]] | [-h]]"
}


##### Main

sample=
stranded=1
normalize=0
dir="/data/projects_3/boehm/holland/data/fastq/raw/renamed"

while [ "$1" != "" ]; do
    case $1 in
        -n | --sample )         shift
                                sample=$1
                                ;;
        -d | --dir )            shift
                                dir=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

if [ -z $sample ]; then
    echo "Error: no sample name."
    usage
    exit 1
fi

if ! [ -d $dir ]; then
    echo "Error: $dir is not a directory."
    exit 1
fi

file1="$dir/${sample}_R1.fastq.gz"
file2="$dir/${sample}_R2.fastq.gz"

if ! [[ -r $file1 && -r $file2 ]]; then
    echo "Error: $file1 or $file2 doesn't exist."
    exit 1
fi


export PATH=$PATH:/package/bowtie-0.12.9:/package/samtools:/home/diehl/tools/cd-hit-v4.6.1-2012-08-27
export PYTHONPATH=$PYTHONPATH:/data/projects_3/diehl/holland/software/lib/python2.7/site-packages/


# --------------------------------------------------------------------------------------------------------------------------
# Trimming and filtering according to khmer protocols

cd /data/processing/diehl/stephen/assembly_pipeline/trim
s1_pe=${sample}_s1_pe
s1_se=${sample}_s1_se
s2_pe=${sample}_s2_pe
s2_se=${sample}_s2_se

# Neither Trimmomatic nor Trim Galore! manage to trim the reads such that the other program doesn't find much to trim anymore.
# Trimmomatic is specifically suggested in the khmer protocol and also mentioned in the Trinity protocol, but it couldn't get rid
# of the adapter contamination in e.g. 130 Blood (as detected with FastQC). Trim Galore get's rid of it, but seems to miss other stuff.
# Instead of relaxing the parameters on one of them too much (trying to get Trimmomatic to trim single adapter sequences specific to
# /1 and /2 resulted in a suspiciously high amount of reads around length 60), I decided to use both with the suggested parameter settings.

# removal of adapter read-through with Trimmomatic
echo "Running Trimmomatic"
java -jar /data/projects_3/diehl/holland/software/Trimmomatic-0.30/trimmomatic-0.30.jar PE -threads 24 ${file1} ${file2} $s1_pe $s1_se $s2_pe $s2_se ILLUMINACLIP:/data/projects/diehl/holland/software/Trimmomatic-0.30/adapters/TruSeq3-PE.fa:2:30:10

# removal of adapter contamination with Trim Galore!
# Trim trailing bases with quality < 15 (suggested by Trinity protocol)
# with k=25 reads shorter than 26 nucleotides are useless
echo "Running Trim Galore!"
/package/trim_galore_v0.3.3/trim_galore -st 2 -q 15 --paired --retain_unpaired --dont_gzip --length 26 -r1 27 -r2 27 $s1_pe $s2_pe &
cat $s1_se $s2_se > ${sample}_se
/package/trim_galore_v0.3.3/trim_galore -st 2 -q 15 --dont_gzip --length 26 ${sample}_se &> /dev/null &
wait
rm -f ${sample}_se $s1_pe $s1_se $s2_pe $s2_se

# Filter reads with quality < 30 over at least 50% of the bases (suggested by khmer protocol)

echo "Quality filtering"
# interleave-reads.py renames the reads to the old Illumina naming with /1 and /2
# that's a very useful side effect, since Trinity's kmer normalisation needs that naming
/data/projects_3/diehl/holland/software/khmer/scripts/interleave-reads.py ${s1_pe}_val_1.fq ${s2_pe}_val_2.fq | /package/fastx_toolkit/fastq_quality_filter -v -Q33 -q 30 -p 50 > ${sample}.pe.qc.fq &

# throw all singles together and manually rename them to /1 and /2
# keeps consistency and allows to use split-paired-reads.py to separate R1 and R2 later on
cat ${sample}_se_trimmed.fq ${s1_pe}_unpaired_1.fq ${s2_pe}_unpaired_2.fq | sed -e 's% %/%' -e 's/:N:.*//' | /package/fastx_toolkit/fastq_quality_filter -v -Q33 -q 30 -p 50 > ${sample}.se.qc.fq &
wait
rm -f ${s1_pe}_val_1.fq ${s2_pe}_val_2.fq ${s1_pe}_unpaired_1.fq ${s2_pe}_unpaired_2.fq ${sample}_se_trimmed.fq

# extract the intact read pairs remaining after quality filtering
/data/projects_3/diehl/holland/software/khmer/scripts/extract-paired-reads.py ${sample}.pe.qc.fq

# add the left over singles to the ones from the trimming step
cat ${sample}.pe.qc.fq.se >> ${sample}.se.qc.fq
rm -f ${sample}.pe.qc.fq ${sample}.pe.qc.fq.se

# Now we have one file with the interleaved intact pairs and one with all the singles
# To also use the strandedness for the single reads, we need to split them up into first and second read again and add them to R1 and R2 respectively of the intact pairs

# Split both files into R1 and R2
/data/projects_3/diehl/holland/software/khmer/scripts/split-paired-reads.py ${sample}.pe.qc.fq.pe &
/data/projects_3/diehl/holland/software/khmer/scripts/split-paired-reads.py ${sample}.se.qc.fq &
wait
rm -f ${sample}.pe.qc.fq.pe ${sample}.se.qc.fq

# Add the singles to the respective pairs
cat ${sample}.se.qc.fq.1 >> ${sample}.pe.qc.fq.pe.1
cat ${sample}.se.qc.fq.2 >> ${sample}.pe.qc.fq.pe.2
rm -f ${sample}.se.qc.fq.1 ${sample}.se.qc.fq.2

file1="/data/processing/diehl/stephen/assembly_pipeline/trim/${sample}_R1.filtered.fastq"
file2="/data/processing/diehl/stephen/assembly_pipeline/trim/${sample}_R2.filtered.fastq"
mv ${sample}.pe.qc.fq.pe.1 ${file1}
mv ${sample}.pe.qc.fq.pe.2 ${file2}

echo "Running FastQC"
/package/FastQC/fastqc -t 24 -o /data/projects_3/boehm/holland/data/fastq/filtered/QC ${file1} ${file2} 

echo "Zipping final fastq files"
pigz -p 12 -9c ${file1} > /data/projects_3/boehm/holland/data/fastq/filtered/${sample}_R1.filtered.fastq.gz &
pigz -p 12 -9c ${file2} > /data/projects_3/boehm/holland/data/fastq/filtered/${sample}_R2.filtered.fastq.gz &
wait

echo "Finished adapter trimming and quality filtering of ${sample}."
echo "Finished adapter trimming and quality filtering of ${sample}." >> /data/processing/diehl/stephen/assembly_pipeline/finished.txt
