#!/bin/bash -x

# Author: Sarah Diehl

# This scripts does a de-novo transcriptome assembly with Trinity.
# Steps in detail:
# - trimming of adapter sequences with Trimmomatic
# - filtering low quality reads and removing low quality trailing bases with fastx_toolkit
# - digital normalisation with Trinity's normalize_by_kmer_coverage.pl (optional, suggested for > 200 mio reads)
# - (stranded) de-novo transcriptome assembly with Trinity
# - ORF prediction with Trinity's transdecoder
# - collapsing identical protein ORF sequences with cd-hit

# References:
# De novo transcript sequence reconstruction from RNA-seq using the Trinity platform for reference generation and analysis. Haas et. al, Nature Protocols 2013
# khmer "Eel Pond" protocol for mRNAseq assembly version 0.8.3 https://khmer-protocols.readthedocs.org/en/v0.8.4/mrnaseq/index.html


function usage
{
    echo "usage: assembly_pipeline.sh -n sample [[[-d directory ] [-s] [-m]] | [-h]]"
    echo "            -m | --normalize {0,1}     do digital normalization no/yes (default: 0)"
    echo "            -s | --stranded  {0,1}     input data is strand-specific no/yes (default: 1)"
}


##### Main

sample=
stranded=1
normalize=0
dir="/data/projects/boehm/holland/project1/data/fastq/filtered/"

while [ "$1" != "" ]; do
    case $1 in
        -n | --sample )         shift
                                sample=$1
                                ;;
        -s | --stranded )       shift
                                stranded=$1
                                ;;
        -m | --normalize )      shift
                                normalize=$1
                                ;;
        -d | --dir )            shift
                                dir=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

if [ -z $sample ]; then
    echo "Error: no sample name."
    usage
    exit 1
fi

if ! [ -d $dir ]; then
    echo "Error: $dir is not a directory."
    exit 1
fi

file1="$dir/${sample}_R1.filtered.fastq"
file2="$dir/${sample}_R2.filtered.fastq"

if ! [[ -r ${file1}.gz && -r ${file2}.gz ]]; then
    echo "Error: ${file1}.gz or ${file2}.gz doesn't exist."
    exit 1
fi

if [ "$stranded" = "1" ]; then
    # in case of a stranded library (in-house data), we tell Trinity and transdecoder about the strand orientations    
	echo "stranded is on"
	trinity_ss="--SS_lib_type RF"
	transdec_ss="-S"
else
	echo "stranded is off"
	trinity_ss=
	transdec_ss=
fi

export PATH=$PATH:/package/bowtie-0.12.9:/package/samtools:/home/diehl/tools/cd-hit-v4.6.1-2012-08-27
export PYTHONPATH=$PYTHONPATH:/data/projects_3/diehl/holland/software/lib/python2.7/site-packages/


# --------------------------------------------------------------------------------------------------------------------------
# Trinity assembly

if ! [ -d /data/extended/Trinity ]; then
    mkdir /data/extended/Trinity
fi

cd /data/extended/Trinity

file1b=`basename ${file1}`
file2b=`basename ${file2}`
pigz -p 12 -dc ${file1}.gz > $file1b &
pigz -p 12 -dc ${file2}.gz > $file2b &
wait

# for data with > 200 mio reads, digital normalisation is adviseable, otherwise Trinity takes much too long
if [ "$normalize" = "1" ]; then
    # normalisation should also know about strandedness
    # we normalise to 30x coverage, as suggested by the Trinity Nature Protocol
    # Be aware that the kmer normalisation needs the reads to be named /1 and /2
    echo "Kmer normalisation"
    /package/trinityrnaseq_r20131110/util/normalize_by_kmer_coverage.pl --seqType fq $trinity_ss --JM 100G --max_cov 30 --left ${file1b} --right ${file2b} --pairs_together --JELLY_CPU 24 --PARALLEL_STATS --output ${sample}_normalized --PE_reads_unordered
    
    file1Norm=${sample}_R1.filtered.fastq.normalized_K25_C30_pctSD200.fq
    file2Norm=${sample}_R2.filtered.fastq.normalized_K25_C30_pctSD200.fq
    
    mv ${dir}/${file1Norm} .
    mv ${dir}/${file2Norm} .

    echo "Trinity assembly"
    /package/trinityrnaseq_r20131110/Trinity.pl --seqType fq $trinity_ss --JM 200G --left ${file1Norm} --right ${file2Norm} --output ${sample} --CPU 24 --bflyCPU 24 --inchworm_cpu 24

    # Cleaning up
    rm -rf ${sample}_normalized/
    #rm -f ${file1}
    #rm -f ${file2}
    echo "Zipping kmer normalised fastqs"
    pigz -p 12 ${file1Norm} &
    pigz -p 12 ${file2Norm} &
    wait
else
    # Assembly without normalisation (chose this whenever possible)
    echo "Trinity assembly without kmer normalisation"
    /package/trinityrnaseq_r20131110/Trinity.pl --seqType fq $trinity_ss --JM 200G --left ${file1b} --right ${file2b} --output ${sample} --CPU 24 --bflyCPU 24 --inchworm_cpu 24
    #rm -f ${file1}
    #rm -f ${file2}
fi

# Adding ${sample} name to fasta ID
sed "s/^>/>${sample}_/" ${sample}/Trinity.fasta > ${sample}_Trinity.fasta

# Copy final renamed assembly to projects
cp ${sample}_Trinity.fasta /data/projects/boehm/holland/project1/data/assembly/per_tissue_sample/${sample}_Trinity.fasta

# Cleaning up (removing all of Trinity's intermediate data which is thousands of files)
rm -rf ${sample}


# --------------------------------------------------------------------------------------------------------------------------
# ORF prediction with Trinity's transdecoder

echo "ORF prediction"
if ! [ -d /data/extended/transdecoder ]; then
    mkdir /data/extended/transdecoder
fi

cd /data/extended/transdecoder
mkdir ${sample}
cd ${sample}

/package/trinityrnaseq_r20131110/trinity-plugins/transdecoder/TransDecoder -t /data/extended/Trinity/${sample}_Trinity.fasta $transdec_ss --CPU 24

# Copy the final raw output of transdecoder (bed, protein and nucleotide sequences of final ORFs) to projects
mkdir /data/projects/boehm/holland/project1/data/ORF_prediction/raw_transdecoder_output/${sample}
cp ${sample}_Trinity.fasta.transdecoder* /data/projects/boehm/holland/project1/data/ORF_prediction/raw_transdecoder_output/${sample}/

# reformat fasta naming of transdecoder proteins
# not necessary with latest Trinity version r20131110
#echo "\$0 ~ /^${sample}/ {print \">\"\$0}" '$0 !~' "/^${sample}/ {print \$0}" > awk.cmd
#cat ${sample}_Trinity.fasta.transdecoder | cut -f10 -d " " | awk -f awk.cmd > ${sample}_ORFp.fa
#rm -f awk.cmd
#cp ${sample}_transdec_p.fa /data/projects/boehm/holland/assembly/stranded_assembly/transdecoder/${sample}/

# collapse identical ORF protein sequences
echo "Collapse ORFs"
~/tools/cd-hit-v4.6.1-2012-08-27/cd-hit -M 0 -i ${sample}_Trinity.fasta.transdecoder.pep -o ../${sample}_ORFp_collapsed.fa -c 1.0 -T 24 -d 0

# Copy collapsed ORF protein sequences to projects
cp ../${sample}_ORFp_collapsed.fa /data/projects/boehm/holland/project1/data/ORF_prediction/protein_sequences_collapsed/
cd ..
rm -rf ${sample}
rm -f ${sample}_ORFp_collapsed.fa*

echo "Finished assembly of ${sample}."
echo "Finished assembly of ${sample}." >> /data/processing/diehl/stephen/assembly_pipeline/finished.txt
