# parse a Blast XML file and produce a tabular report
from Bio.Blast import NCBIXML
import sys
import re

if "snakemake" in locals():
    infile = str(snakemake.input)
    outfile = str(snakemake.output)
else:
    infile = str(sys.argv[1])
    outfile = str(sys.argv[2])

outfmt = "{query}\t{q_len}\t{subject}\t{ali_len}\t{ident}\t{perc_ident}\t{q_start}\t{q_end}\t{s_start}\t{s_end}\t{evalue}\t{bitscore}\t{q_seq}\t{s_seq}\t{match}\n"

with open(infile, "r") as infh, open(outfile, "w") as outfh:

    # write header line
    outfh.write(outfmt.format(query = "#query title",
                        q_len = "query length",
                        subject = "subject title",
                        ali_len = "alignment length",
                        ident = "identities",
                        perc_ident = "percent identity",
                        q_start = "start on query",
                        q_end = "end on query",
                        s_start = "start on subject",
                        s_end = "end on subject",
                        evalue = "e-value",
                        bitscore = "bit score",
                        q_seq = "query sequence",
                        s_seq = "subject sequence",
                        match = "matches"))

    blast_records = NCBIXML.parse(infh)

    for record in blast_records:
        # "NA" for queries without hits
        if len(record.alignments) == 0:
            outfh.write(outfmt.format(query = record.query,
                        q_len = record.query_length,
                        subject = "NA",
                        ali_len = "NA",
                        ident = "NA",
                        perc_ident = "NA",
                        gapopen = "NA",
                        q_start = "NA",
                        q_end = "NA",
                        s_start = "NA",
                        s_end = "NA",
                        evalue = "NA",
                        bitscore = "NA",
                        q_seq = "NA",
                        s_seq = "NA",
                        match = "NA"
                        ))
        for alignment in record.alignments:
            for hsp in alignment.hsps:
                outfh.write(outfmt.format(query = record.query,
                            q_len = record.query_length,
                            subject = re.sub(" ", "|", alignment.title, count = 1),
                            ali_len = hsp.align_length,
                            ident = hsp.identities,
                            perc_ident = (hsp.identities / hsp.align_length) * 100,
                            gapopen = hsp.gaps,
                            q_start = hsp.query_start,
                            q_end = hsp.query_start + len(hsp.query),
                            s_start = hsp.sbjct_start,
                            s_end = hsp.sbjct_start + len(hsp.sbjct),
                            evalue = hsp.expect,
                            bitscore = hsp.score,
                            q_seq = hsp.query,
                            s_seq = hsp.sbjct,
                            match = hsp.match
                            ))
