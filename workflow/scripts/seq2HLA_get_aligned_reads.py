from operator import itemgetter
import sys
import linecache
import ast
import os
from Bio import SeqIO
import numpy
import operator
from optparse import OptionParser

#These variables need to be global, as they are filled and used by different modules
readcount={}
readspergroup={}
allelesPerLocus={}

def main(runName,fastaClassI,fastaClassII):
    bowtiebuild = "references/"+runName
    bowtiebuildorf = "references/"+runName+"_ORFs"
    
    #call HLA typing for Class I
    map=createRefDict(fastaClassI,"A","B","C")
    removeList = createRemoveList(runName+"-ClassI",map)
    removeReads(runName+"-ClassI",removeList)
    
    mappinglog = runName+"-mapping.log"
    mappinghandle = open(mappinglog,'w')
    countslog = runName+"-mappingCounts.log"
    countshandle = open(countslog,'w')
    
    for allele in removeList.keys():
        readFile1=runName+"-ClassI-reads_"+allele.replace("*","")+"_1.fq"
        readFile2=runName+"-ClassI-reads_"+allele.replace("*","")+"_2.fq"
        sam = runName+"-ClassI-"+allele.replace("*","")+".sam"
        mappinghandle.write(mapping(sam, runName, readFile1, readFile2, bowtiebuild)+"\n")
        cmd = "/package/samtools/samtools view -S %s | cut -f 3 | sort | uniq -c" % (sam)
        countOutput=os.popen(cmd).read()
        countshandle.write(cmd+"\n"+countOutput)
        
        sam = runName+"-ClassI-"+allele.replace("*","")+"_ORFs.sam"
        mappinghandle.write(mapping(sam, runName, readFile1, readFile2, bowtiebuildorf)+"\n")
        cmd = "/package/samtools/samtools view -S %s | cut -f 3 | sort | uniq -c" % (sam)
        countOutput=os.popen(cmd).read()
        countshandle.write(cmd+"\n"+countOutput)
        
    #call HLA typing for Class II
    map=createRefDict(fastaClassII,"DQA1","DQB1","DRB1")
    removeList = createRemoveList(runName+"-ClassII",map)
    removeReads(runName+"-ClassII",removeList)
    
    for allele in removeList.keys():
        readFile1=runName+"-ClassII-reads_"+allele.replace("*","")+"_1.fq"
        readFile2=runName+"-ClassII-reads_"+allele.replace("*","")+"_2.fq"
        sam = runName+"-ClassII-"+allele.replace("*","")+".sam"
        mappinghandle.write(mapping(sam, runName, readFile1, readFile2, bowtiebuild)+"\n")
        cmd = "/package/samtools/samtools view -S %s | cut -f 3 | sort | uniq -c" % (sam)
        countOutput=os.popen(cmd).read()
        countshandle.write(cmd+"\n"+countOutput)
        
        sam = runName+"-ClassII-"+allele.replace("*","")+"_ORFs.sam"
        mappinghandle.write(mapping(sam, runName, readFile1, readFile2, bowtiebuildorf)+"\n")
        cmd = "/package/samtools/samtools view -S %s | cut -f 3 | sort | uniq -c" % (sam)
        countOutput=os.popen(cmd).read()
        countshandle.write(cmd+"\n"+countOutput)
    
    mappinghandle.close()
    countshandle.close()


#performs the bowtie mapping for the 2 iterations using the given parameters
def mapping(sam,runName,readFile1,readFile2,bowtiebuild):
    cmd = "/package/bowtie2-2.1.0/bowtie2 -X 1000 --no-mixed --no-discordant -a --threads 6 -x "+bowtiebuild+" -1 "+readFile1+" -2 "+readFile2+" > "+sam
    print cmd
    mappingOutput=os.popen(cmd).read()      
    print mappingOutput
    return cmd+"\n"+mappingOutput

#create dictionary "map", that contains all IMGT/HLA-allele names as keys and the allele name (e.g. A*02:01:01) as value
#dictionary "readcount" is initialized with 0 for each allele
#dictionary "readspergroup" is initialized with 0 for each group (2digit, e.g. A*01)
#dictionary "allelesPerLocus" stores the number of alleles per locus.
def createRefDict(hlafasta,locus1,locus2,locus3):
    map={}
    allelesPerLocus[locus1]=0
    allelesPerLocus[locus2]=0
    allelesPerLocus[locus3]=0
    handle=open(hlafasta,'r')
    for record in SeqIO.parse(handle, "fasta") :
        l=record.description.split(' ')
        hlapseudoname=l[0]
        hlaallele=l[1]
        map[hlapseudoname]=hlaallele
        readcount[hlaallele]=0
        readspergroup[hlaallele.split(":")[0]]=0
        allelesPerLocus[hlaallele.split('*')[0]]+=1
    handle.close()
    return map

#open mapping file and all read ids to the list "removeList", which map to one of the three groups in "alleles"
def createRemoveList(runName,map):
    removeList={}
    alleles = []
    sam=runName+"-iteration1.sam"
    alleles_in=runName+".digitalhaplotype1"
    
    allele1 = linecache.getline(alleles_in, 2).split('\t', 2)[1]
    alleles.append(allele1)
    removeList[allele1]={}
    allele2 = linecache.getline(alleles_in, 3).split('\t', 2)[1]
    alleles.append(allele2)
    removeList[allele2]={}
    allele3 = linecache.getline(alleles_in, 4).split('\t', 2)[1]
    alleles.append(allele3)
    removeList[allele3]={}

    samhandle=open(sam,'r')
    for line in samhandle:
        if line[0]!='@':
            illuminaid=line.split("\t")[0]
            hlapseudoname = line.split("\t")[2]
            allelename = map[hlapseudoname].split(':')[0]
            if allelename in alleles:
                removeList[allelename][illuminaid]=1
    samhandle.close()
    
    sam=runName+"-iteration2.sam"
    alleles_in=runName+".digitalhaplotype2"
    
    allele4 = linecache.getline(alleles_in, 2).split('\t', 2)[1]
    if allele4 != "no":
        alleles.append(allele4)
        removeList[allele4]={}
    allele5 = linecache.getline(alleles_in, 3).split('\t', 2)[1]
    if allele5 != "no":
        alleles.append(allele5)
        removeList[allele5]={}
    allele6 = linecache.getline(alleles_in, 4).split('\t', 2)[1]
    if allele6 != "no":
        alleles.append(allele6)
        removeList[allele6]={}

    samhandle=open(sam,'r')
    for line in samhandle:
        if line[0]!='@':
            illuminaid=line.split("\t")[0]
            hlapseudoname = line.split("\t")[2]
            allelename = map[hlapseudoname].split(':')[0]
            if allelename in alleles:
                removeList[allelename][illuminaid]=1
    samhandle.close()
    return removeList

#Remove reads that mapped to the three top-scoring alleles and write the remaining reads into two new read files
def removeReads(runName,removeList):
    aligned1=runName+"_1.aligned"
    aligned2=runName+"_2.aligned"
    
    for allele in removeList.keys():
        subRemoveList = removeList[allele]
        newReadFile1=runName+"-reads_"+allele.replace("*","")+"_1.fq"
        newReadFile2=runName+"-reads_"+allele.replace("*","")+"_2.fq"
        #r1 and r2, which are the input of bowtie in the 2nd iteration
        r1=open(newReadFile1,"w")
        r2=open(newReadFile2,"w")
        #open the 2 files, that contain the reads that mapped in the 1st iteration
        aligned_handle1=open(aligned1,"r")
        aligned_handle2=open(aligned2,"r")
    
        #One read entry consists of 4 lines: header, seq, "+", qualities.
        for record in SeqIO.parse(aligned_handle1, "fastq"):
            illuminaid=record.id.split('/')[0].split(' ')[0]#find exact id, which also appears in the mapping file
            if illuminaid in subRemoveList:
                SeqIO.write(record, r1, "fastq")
    
        for record in SeqIO.parse(aligned_handle2, "fastq"):
            illuminaid=record.id.split('/')[0].split(' ')[0]#find exact id, which also appears in the mapping file
            if illuminaid in subRemoveList:
                SeqIO.write(record, r2, "fastq")

            
if __name__ == '__main__':
    parser = OptionParser(usage="usage: %prog -r runName", version="%prog 1.0")
    parser.add_option("-r", "--runName",
            action="store", 
            dest="runName",
            help="Name of this HLA typing run. Wil be used throughout this process as part of the name of the newly created files.")


    (options, args) = parser.parse_args()
    if not options.runName:   
        parser.error('Run name not given.')
    runName=options.runName
    fastaClassI="/data/processing/petersen/projects/ines-RE-pipeline/workflow/envs/seq2hla.env/share/seq2hla-2.2-2/references/ClassIWithoutNQex2-3.plus75.fasta"
    fastaClassII="/data/processing/petersen/projects/ines-RE-pipeline/workflow/envs/seq2hla.env/share/seq2hla-2.2-2/references/HLA2.ex2.plus75.fasta"
    #as shown in the publication HLA typing with RNA-Seq works best by allowing as less mismatches as necessary

    main(runName,fastaClassI,fastaClassII)
