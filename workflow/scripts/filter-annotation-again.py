#!/usr/bin/env python

import sys
import csv

if "snakemake" in locals():
    keystrings_file = str(snakemake.input.filter_strings)
    table_file = str(snakemake.input.table)
    output_file = str(snakemake.output)
else:
    keystrings_file = sys.argv[1]
    table_file = sys.argv[2]
    output_file = sys.argv[3]

# Read the list of keystrings
keystrings = [ ]
with open(keystrings_file, "r") as f:
    keystrings = f.read().splitlines()
    f.close()

# Print out lines of the table where column 4 doesn't match any keystring
with open(table_file, "r") as tf, open(output_file, "w") as out:
    tsv_in = csv.reader(tf, delimiter = "\t")
    tsv_out = csv.writer(out, delimiter = "\t")
    for row in tsv_in:
        if not any(keystring in row[3] for keystring in keystrings):
            tsv_out.writerow(row)

