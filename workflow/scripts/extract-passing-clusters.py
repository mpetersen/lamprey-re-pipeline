from pathlib import Path
from shutil import copyfile
import sys
import re
import csv

if "snakemake" in locals():
    output_passed_list = snakemake.output.passed_list
    input = snakemake.input
else:
    input = sys.argv[1]
    output_passed_list = sys.argv[2]
    output_fasta_dir = sys.argv[3]

print("Reading stats file...")
with open(str(output_passed_list), "w") as outfh, open(str(input), "r") as infh:
    rows = csv.DictReader(infh, dialect = "excel-tab")
    for row in rows:
        if row["failed filter"] == "None":
            print("Extracting cluster {cluster}".format(cluster = row["name"]))
            outfh.write("{cluster}\n".format(cluster = row["name"]))
            # Copy alignment file
            source = Path("results/111_extract_proteinortho_clusters/unaligned/{cluster}.fa".format(cluster = row["name"]))
            destination = Path("{output_fasta_dir}/{cluster}.fa".format(output_fasta_dir = output_fasta_dir, cluster = row["name"]))
            copyfile(source, destination)
outfh.close()

print("All done.")
