#!/usr/bin/env python3

import sys
import sqlite3
import csv
import re

if "snakemake" in locals():
    db_file     = snakemake.input.db
    go_file     = snakemake.input.go
    filter_file = snakemake.input.filter_keystrings
    ok_file     = snakemake.output
else:
    db_file     = sys.argv[1]
    go_file     = sys.argv[2]
    filter_file = sys.argv[3]
    ok_file     = sys.argv[4]

#
# Create all tables
#
sql_queries = [ ]
# first drop all tables
sql_queries.append("DROP TABLE IF EXISTS go")
sql_queries.append("DROP TABLE IF EXISTS filters")
sql_queries.append("DROP TABLE IF EXISTS filter_flags")
sql_queries.append("DROP TABLE IF EXISTS filter_keystrings")
sql_queries.append("DROP TABLE IF EXISTS go_rowids")
sql_queries.append("DROP VIEW IF EXISTS go_filtered")

# then create them anew
sql_queries.append("""
    CREATE TABLE IF NOT EXISTS go (
        rowid INTEGER PRIMARY KEY AUTOINCREMENT,
        True TEXT,
        Tags TEXT,
        Description TEXT,
        Length INTEGER NOT NULL,
        SeqName TEXT NOT NULL,
        Hits INTEGER NOT NULL,
        e_Value REAL NOT NULL,
        sim_mean REAL NOT NULL,
        Number_GO_IDs INTEGER NOT NULL,
        GO_IDs TEXT,
        GO_Names TEXT,
        Enzyme_Codes TEXT,
        Enzyme_Names TEXT,
        InterPro_IDs TEXT,
        InterPro_GO_IDs TEXT,
        InterPro_GO_Names TEXT,
        filter_flags INTEGER,
        f_blast_hit INTEGER,
        f_ipr_hit INTEGER,
        f_matches_keystring INTEGER,
        f_uncharacterized INTEGER,
        f_plasma_membrane INTEGER,
        f_transmembrane INTEGER,
        f_transporter_protein INTEGER,
        f_keystrings INTEGER
    );
    """)

sql_queries.append("""
    CREATE TABLE IF NOT EXISTS filters (
        rowid INTEGER PRIMARY KEY AUTOINCREMENT,
        flag INTEGER NOT NULL,
        description TEXT NOT NULL
    );
    """)

sql_queries.append("""
    CREATE TABLE IF NOT EXISTS filter_flags (
        rowid INTEGER PRIMARY KEY AUTOINCREMENT,
        flag INTEGER NOT NULL,
        filter_keystrings_remove INTEGER NOT NULL DEFAULT 0,
        filter_unchar_without_tm_remove INTEGER NOT NULL DEFAULT 0,
        filter_unchar_with_tm_keep INTEGER NOT NULL DEFAULT 0,
        filter_transporter_remove INTEGER NOT NULL DEFAULT 0,
        filter_cell_membrane_keep INTEGER NOT NULL DEFAULT 0
    );
    """)

sql_queries.append("""
    CREATE TABLE IF NOT EXISTS filter_keystrings (
        rowid INTEGER PRIMARY KEY AUTOINCREMENT,
        keystring TEXT NOT NULL
    );
    """)

sql_queries.append("""
    INSERT INTO filters
        (flag, description)
    VALUES
        (1, "keystrings: remove"),
        (2, "uncharacterized without TM domain: remove"),
        (4, "uncharacterized with TM domain: keep"),
        (8, "transporter protein: remove"),
        (16, "plasma membrane localisation: keep")
    ;
    """)

sql_queries.append("""
    INSERT INTO filter_flags
        (flag, filter_keystrings_remove, filter_unchar_without_tm_remove, filter_unchar_with_tm_keep, filter_transporter_remove, filter_cell_membrane_keep)
    VALUES
        (0, 0, 0, 0, 0, 0),
        (1, 1, 0, 0, 0, 0),
        (2, 0, 1, 0, 0, 0),
        (3, 1, 1, 0, 0, 0),
        (4, 0, 0, 1, 0, 0),
        (5, 1, 0, 1, 0, 0),
        (6, 0, 1, 1, 0, 0),
        (7, 1, 1, 1, 0, 0),
        (8, 0, 0, 0, 1, 0),
        (9, 1, 0, 0, 1, 0),
        (10, 0, 1, 0, 1, 0),
        (11, 1, 1, 0, 1, 0),
        (12, 0, 0, 1, 1, 0),
        (13, 1, 0, 1, 1, 0),
        (14, 0, 1, 1, 1, 0),
        (15, 1, 1, 1, 1, 0),
        (16, 0, 0, 0, 0, 1),
        (17, 1, 0, 0, 0, 1),
        (18, 0, 1, 0, 0, 1),
        (19, 1, 1, 0, 0, 1),
        (20, 0, 0, 1, 0, 1),
        (21, 1, 0, 1, 0, 1),
        (22, 0, 1, 1, 0, 1),
        (23, 1, 1, 1, 0, 1),
        (24, 0, 0, 0, 1, 1),
        (25, 1, 0, 0, 1, 1),
        (26, 0, 1, 0, 1, 1),
        (27, 1, 1, 0, 1, 1),
        (28, 0, 0, 1, 1, 1),
        (29, 1, 0, 1, 1, 1),
        (30, 0, 1, 1, 1, 1),
        (31, 1, 1, 1, 1, 1)
    ;
    """)

# open connection and execute all queries
conn = sqlite3.connect(str(db_file))
c = conn.cursor() # Cursor object executes queries
for query in sql_queries:
    c.execute(query)
conn.commit()


sql_insert = """
    INSERT INTO go
        (True, Tags, Description, Length, SeqName, Hits, e_Value, sim_mean, Number_GO_IDs, GO_IDs, GO_Names, Enzyme_Codes, Enzyme_names, InterPro_IDs, InterPro_GO_IDs, InterPro_GO_Names, filter_flags, f_blast_hit, f_ipr_hit, f_matches_keystring, f_uncharacterized, f_plasma_membrane, f_transmembrane, f_transporter_protein)
    VALUES
        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0, 0, 0, 0, 0, 0, 0, 0);
    """

# import the GO annotation table
print("Importing GO annotation table")
with open(str(go_file), "r") as infh:
    rows = csv.reader(infh, delimiter = "\t")
    hdr = next(rows) # skip header line
    for row in rows:
        # clean up some before inserting
        row[2] = re.sub(" isoform X?[0-9]+$", "", row[2]) # collapse isoforms
        row[2] = re.sub("^(XP_|[A-Z]+)[0-9]+\.[0-9]", "", row[2]) # remove accessions
        row[2] = re.sub("-like", "", row[2]) # remove "like" forms
        row[2] = row[2].strip() # remove leading whitespace
        c.execute(sql_insert, (row))
    conn.commit()

print("Setting filter flags 2-5")
# set the filter flags
queries = [ ]
queries.append("UPDATE go SET f_blast_hit = 1 WHERE Tags LIKE '%BLASTED%'")
queries.append("UPDATE go SET f_ipr_hit = 1 WHERE InterPro_GO_IDs != 'no IPR match'")
queries.append("UPDATE go SET f_uncharacterized = 1 WHERE Description LIKE '%uncharacterized%' OR Description LIKE '%hypothetical%'")
queries.append("UPDATE go SET f_plasma_membrane = 1 WHERE GO_Names LIKE '%C:plasma membrane%' OR InterPro_GO_Names LIKE '%C:plasma membrane%'")
queries.append("UPDATE go SET f_transmembrane = 1 WHERE InterPro_IDs LIKE '%TMhelix%'")
queries.append("UPDATE go SET f_transporter_protein = 1 WHERE Description LIKE '%transporter%' OR Description LIKE '%carrier%' OR Description LIKE '%channel%' OR Description LIKE '%exchanger%'")
queries.append("UPDATE go SET filter_flags = filter_flags + 16 WHERE GO_Names LIKE '%C:plasma membrane%' OR GO_Names LIKE '%C:membrane%' OR InterPro_GO_Names LIKE '%C:plasma membrane%' OR InterPro_GO_Names LIKE '%C:membrane%'")
queries.append("UPDATE go SET filter_flags = filter_flags + 8 WHERE Description LIKE '%transporter%' OR Description LIKE '%carrier%' OR Description LIKE '%channel%' OR Description LIKE '%exchanger%'")
queries.append("UPDATE go SET filter_flags = filter_flags + 4 WHERE ( Description like '%uncharacterized%' OR Description LIKE '%hypothetical%') AND InterPro_IDs LIKE '%TMHMM%'")
queries.append("UPDATE go SET filter_flags = filter_flags + 2 WHERE ( Description like '%uncharacterized%' OR Description LIKE '%hypothetical%') AND NOT InterPro_IDs LIKE '%TMHMM%'")

for query in queries:
    c.execute(query)
conn.commit()

# last flag: the keyword list
print("Setting filter flag 1")
query = "INSERT INTO filter_keystrings (keystring) VALUES (?)"
with open(str(filter_file), "r") as infh:
    next(infh) # skip header line
    for line in infh:
        keystring = line.strip().split("\t")[0] # don't care about other columns
        c.execute(query, (keystring, )) # for some reason this needs a comma
    conn.commit()

query = """
    CREATE TABLE
        go_rowids
    AS SELECT
        go.rowid
    FROM
        go
    INNER JOIN
        filter_keystrings
    ON
        go.Description LIKE '%' || filter_keystrings.keystring || '%'
    """
c.execute(query)
query = "UPDATE go SET filter_flags = filter_flags + 1, f_matches_keystring = 1 WHERE rowid IN (SELECT rowid FROM go_rowids)"
c.execute(query)

# Create filtered view
query = """
    CREATE VIEW
        go_filtered
    AS SELECT
        *
    FROM
        go
    INNER JOIN
        filter_flags ON go.filter_flags = filter_flags.flag
    WHERE
        ((filter_keystrings_remove  = 0 AND filter_unchar_without_tm_remove = 0) OR filter_unchar_with_tm_keep = 1) AND filter_transporter_remove = 0
    ORDER BY
        go.rowid
    """
c.execute(query)
conn.commit()
conn.close()

print("Done")
ofh = open(str(ok_file), "w")
ofh.close()
