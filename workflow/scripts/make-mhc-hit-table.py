#!/usr/bin/env python3

import csv
import re

cluster_hits = dict()

def which_mhc_class(id):
    id = re.sub("^.+\|", "", id)
    if re.match("^HLA", id): return 1
    return 2

with open(snakemake.input[0]) as tsv:
    rows = csv.reader(tsv, delimiter = "\t")

    for row in rows:
        if row[0].startswith("#"): continue # skip comment lines
        cluster = re.sub(":.+$", "", row[0])
        mhc_class = which_mhc_class(row[1])
        if not cluster in cluster_hits: # create new dict if not encountered before
            cluster_hits[cluster] = { mhc_class: { "identity": row[2], "evalue": row[-2], "score": row[-1] } }
        else:
            if not mhc_class in cluster_hits[cluster]: # add to dict if not encountered before
                cluster_hits[cluster][mhc_class] = { "identity": row[2], "evalue": row[-2], "score": row[-1] }

with open(snakemake.output[0], "w") as outfh:
    outfh.write("cluster\tmhc_class\tperc_ident\tevalue\tscore\n")
    for cluster in cluster_hits:
        for mhc in cluster_hits[cluster]:
            outfh.write("{cluster}\t{mhc}\t{perc_ident}\t{evalue}\t{score}\n".format(cluster = cluster, mhc = mhc, perc_ident = cluster_hits[cluster][mhc]["identity"], evalue = cluster_hits[cluster][mhc]["evalue"], score = cluster_hits[cluster][mhc]["score"]))
