from Bio import SeqIO
from pathlib import Path
import re
import sys

if "snakemake" in locals():
    input_passed_list = snakemake.input.passed_list
    input_fasta_dir = snakemake.input.fasta_dir
    output_species_per_cluster_table = snakemake.output.species_per_cluster_table
    output_fasta_dir = snakemake.output.fasta_dir
else:
    input_passed_list = sys.argv[1]
    input_fasta_dir = sys.argv[2]
    output_species_per_cluster_table = sys.argv[3]
    output_fasta_dir = sys.argv[4]

print("Reading list of passing clusters")
with open(str(output_species_per_cluster_table), "w") as outfh, open(str(input_passed_list), "r") as infh:
    outfh.write("cluster\tindividuals\n")
    for cluster in infh:
        cluster = cluster.strip()
        longest = ""
        length_longest = 0
        individuals = { }
        input_file = Path(str(input_fasta_dir)) / Path("{cluster}.fa".format(cluster = cluster))
        for record in SeqIO.parse(str(input_file), "fasta"):
            indiv = re.sub("_.+", "", record.id) # reduce to individual
            if indiv in individuals:
                individuals[indiv] += 1
            else:
                individuals[indiv] = 1
            seq_length = len(record.seq.ungap("-")) # length without gaps, of course
            if seq_length > length_longest:
                longest = record
                length_longest = seq_length
        outfh.write("{filename}\t{indivs}\n".format(filename = str(input_file), indivs = len(individuals)))
        if len(individuals) >= 5:
            output_file = Path(str(output_fasta_dir)) / Path("{cluster}.fa".format(cluster = str(cluster)).name
            longest.seq = longest.seq.ungap("-") # ungap sequence
            longest.id = "{cluster}_{rest}".format(cluster = cluster, rest = longest.id)
            with open(str(output_file), "w") as seq_out:
                SeqIO.write(longest, seq_out, "fasta")
                seq_out.close()

print("All done.")
