#!/usr/bin/python
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
import sys


def getFamily(name):
    return "_".join(name.split("_")[0:4])
   
def main():
    seqs = []
    lengths = {}    
    inFile = sys.argv[1]
    input_handle = open(inFile, "r")
    for record in SeqIO.parse(input_handle, "fasta"):
        family = getFamily(record.id)
        #print >> sys.stderr, "%s has family %s and length %s" % (record.id, family, len(record))
        if family in lengths:
            if len(record) > lengths[family]:
                lengths[family] = len(record)
                #print >> sys.stderr, "Family length updated"
        else:
            lengths[family] = len(record)
        seqs.append(record)
    input_handle.close()
    
    for record in seqs:
        if (len(record) >= (lengths[getFamily(record.id)] * 0.5)):
            SeqIO.write(record, sys.stdout, "fasta")
            #print >> sys.stderr, "%s written out." % (record.id)

if __name__ == '__main__':
    main()
