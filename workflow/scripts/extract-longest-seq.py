#!/usr/bin/env python3

import os
import re
from Bio import SeqIO

maxlen = 0
longest = SeqIO.SeqRecord(seq = "")

clustername = os.path.basename(snakemake.input[0]).replace(".fa", "")

# identify the longest sequence
records = SeqIO.parse(snakemake.input[0], "fasta")
for record in records:
    if len(record.seq) > maxlen:
        longest = record
        maxlen = len(longest.seq)

# found the longest seq, write to file
with open(snakemake.output[0], "w") as outfh:
    longest.id = str(clustername) + ":" + str(longest.id)
    SeqIO.write(longest, outfh, "fasta")

