#!/usr/bin/python
# this script reads in ClustalW alignments and computes a polymorphism score
# Written by Sarah PETER <sarah.peter@uni.lu> in 2014
# Slightly modified by Malte Petersen <petersen@ie-freiburg.mpg.de> in 2020, while shaking head

import sys

from Bio import AlignIO
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
import numpy
#from pylab import *
import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt
import os.path
from pathlib import Path
from argparse import ArgumentParser

def parseArgs():
    parser = ArgumentParser(description="This scripts reads a multiple sequence alignment, computes the Simpson index (position-wise polymorphism scores) and plots them if they pass a set of filter thresholds")
    parser.add_argument("-i", "--input-dir", dest="input_dir", required=True, help="Input alignment file")
    parser.add_argument("-o", "--output-dir", dest="output_dir", required=True, help="Output directory")
    parser.add_argument("-b", "--window-boundary-size", dest="window_boundary_size", type=int, default=3, required=False, help="Number of consecutive positions in the alignment where all sequences are identical, to define the start and end of the scoring window (see --minimum-window-size).")
    parser.add_argument("-w", "--minimum-window-size", dest="minimum_window_size", type=float, default=0.5, required=False, help="Minimum fraction of the alignment that the scoring window (see --window-boundary-size) needs to span")
    parser.add_argument("-l", "--minimum-window-length", dest="minimum_window_length", type=int, default=50, required=False, help="Minimum length (aa positions) of the scoring window (see --window-boundary-size and --minimum-window-size)")
    options = parser.parse_args()

    return options

def makeLetterCount(x):
    def letterCount(s):
        return x.count(s)
    return letterCount


def computeColScore(col):
    chars = set(col)
    if "-" in chars:
        chars.remove("-")

    # Wu-kabat Variability coefficient
    # N is the number of sequences in the alignment
    # k is the number of different amino acids at a given position and
    # n is the times that the most common amino acid at that position is present.
    # http://imed.med.ucm.es/PVS/pvs-help.html#vmth
    f = makeLetterCount(col)
    #N = len(col)
    #k = len(chars)
    #n = max(map(f,chars))
    #wukabat = (N*k)/n

    # Simpson index
    s = 0.0
    chars = list(chars)
    counts = list(map(f,chars))
    n = sum(counts)
    for i in range(len(chars)):
        s += counts[i]*(counts[i]-1.0)
    if (n <= 1):
        simpson = 0
    else:
        simpson = 1.0 - s/(n*(n-1.0))
    return simpson

def computeWindowScore(colScores):
    return sum(colScores)

def plotColAA(col, pos, AAColors):
    chars = set(col)
    if "-" in chars:
        chars.remove("-")
    chars = list(chars)
    f = makeLetterCount(col)
    counts = list(map(f,chars))
    x = {}
    for i in range(len(chars)):
        aa = chars[i]
        count = counts[i]
        prop = float(count)/len(col)
        if prop in x:
            x[prop] += 1
            plt.text(pos,prop-0.05,aa, color=AAColors[aa])
        else:
            x[prop] = 1
            plt.text(pos,prop,aa, color=AAColors[aa])


def plotPolymorphism(inFile, outdir=".", window_boundary_size = 3, minimum_window_size = 0.3):

    LOG = ""
    stats = {   "name": "",
                "alignment length": 0,
                "sequences": 0,
                "scoring window start": None,
                "scoring window end": None,
                "failed filter": None,
                "position scores": [],
                "peaks": 0,
                "valleys": 0
                }

    AA = ["A","R","N","D","C","E","Q","G","H","I","L","K","M","F","P","S","T","W","Y","V","U","O","B","Z","J","X"]
    AAColors = {}
    NUM_COLORS = len(AA)
    cm = plt.get_cmap('gist_rainbow')
    for i in range(NUM_COLORS):
        AAColors[AA[i]] = cm(1.*i/NUM_COLORS)

    windowSize = 30
    shift = 20
    # number of columns where the alignment needs to match
    # that define the start and end of the window for score computation
    matchCols = window_boundary_size
    minimum_window_size = minimum_window_size # minimum fraction of the alignment that the scoring window has to span

    name = os.path.splitext(os.path.basename(inFile))[0]
    stats["name"] = name
    LOG += "Processing %s ...\n" % (inFile)
    print("Reading alignment " + str(inFile))
    try: aln = AlignIO.read(inFile, "fasta")  ## using FASTA format for alignments now
    except ValueError:
        print("error file: " + inFile)
    alnLength = len(aln[0])
    stats["alignment length"] = alnLength
    alnDepth = len(aln)
    stats["sequences"] = alnDepth

    # find first position where all seqs match
    ## MP this part is overly complicated. Just use a flag variable to track
    ## how many consecutive positions are identical.
    start = -1
    i = 1
    l = [5]*matchCols
    while start == -1:
        if i >= alnLength:
            LOG += "Error: No %s columns with perfect alignment found. Could not start scoring.\n" % (matchCols)
            stats["failed filter"] = 0
            return LOG, stats
        col = aln[:,i-1]
        chars = set(col)
        del(l[0])
        l.append(len(chars)-1)
        if l == [0]*matchCols:
            start = i-2
        i += 1
    stats["scoring window start"] = start

    # find last position where all seqs match
    end = -1
    i = alnLength
    l = [5]*matchCols
    while end == -1:
        col = aln[:,i-1]
        chars = set(col)
        del(l[0])
        l.append(len(chars)-1)
        if l == [0]*matchCols:
            end = i+2
        i -= 1

    stats["scoring window end"] = end

    if (start + minimum_window_length >= end):
        LOG += "Didn't pass filter 1: Scoring window too small (< " + str(minimum_window_length) + ").\n"
        stats["failed filter"] = 1
        return LOG, stats
    elif ((end-start)/float(alnLength) < float(minimum_window_size)):
        LOG += "Didn't pass filter 2: Scoring window spans less than %d%% of the total alignment length.\n" % (float(minimum_window_size) * 100)
        stats["failed filter"] = 2
        return LOG, stats

    title = "%s | position %s to %s (of %s) | %s sequences" % (name, start,end,alnLength,alnDepth)
    LOG += title + "\n"
    plotWidth = min(0.1 * (end-start), 204)
    plt.figure(figsize=(plotWidth,6), dpi=160)
    plt.suptitle(title)
    plt.subplot(212)
    plt.xlim(0,end-start+1)
    plt.ylim(-0.1,1.1)
    plt.ylabel("AA proportion")
    plt.xlabel("alignment position")

    offset = 10-(start%10)
    ticks = plt.np.arange(offset, end-start, 10)
    labels = list(range(start+offset, end, 10))
    plt.xticks(ticks, labels)


    allColScores = []
    windowScores = []
    colScores = [0]*windowSize
    for i in range(start,end+1):
        col = aln[:,i-1]
        score = computeColScore(col)
        plotColAA(col,i-start,AAColors)
        allColScores.append(score)
        del(colScores[0])
        colScores.append(score)
        if (i%5 == 0):
            plt.plot([i-start,i-start],[-0.1,1.1], "-", color="lightgrey")
        if (i%shift == 0):
            ws = computeWindowScore(colScores)
            windowScores.append(ws)

    plt.subplot(2,1,1)
    plt.xlim(0,end-start+1)
    plt.ylim(0.0,1.0)
    plt.ylabel("simpson index")
    plt.xticks(ticks, labels)
    plt.plot(allColScores)
    stats["position scores"] = allColScores
    #print len(set(allColScores))
    outName = "%s/%s_prop.png" % (outdir,name)

    peak_values =  list(i for i in allColScores if (i > 0.5))
    peaks = len(peak_values)
    stats["peaks"] = peaks
    valleys = sum([x < 0.1 for x in allColScores])
    stats["valleys"] = valleys
    nr = len(allColScores)

    if (peaks < 5):
        LOG += "Didn't pass filter 3: less than 5 peaks.\n"
        stats["failed filter"] = 3

    elif (peaks > 0.3*nr and len(set(allColScores)) <= 2):
        LOG += "Didn't pass filter 4: too many peaks (> 30%) and all the same height.\n"
        stats["failed filter"] = 4

    elif (peaks > 0.5*nr):
        LOG += "Didn't pass filter 5: too many peaks (> 50%).\n"
        stats["failed filter"] = 5

    elif (valleys < 0.2*nr) and (min(windowScores) > 0.2):
        LOG += "Didn't pass filter 6: not enough valleys (< 20%) and no long stretch of almost perfect match (no window with score < 0.2).\n"
        stats["failed filter"] = 6
    elif (alnDepth > 4 and len(set(peak_values)) <= 1):
        peak_positions = list(i for i in range(len(allColScores)) if (allColScores[i] > 0.5))
        filter7 = True
        for i in peak_positions:
            col = aln[:,i+start]
            chars = set(col)
            if "-" in chars:
                chars.remove("-")
            if len(chars) > 2:
                filter7 = False
                break
        if filter7:
            LOG += "Didn't pass filter 7: peaks are made up of only 2 amino acids and always at the same proportion.\n"
            stats["failed filter"] = 7
        else:
            try:
                plt.tight_layout()
            except ValueError:
                LOG += "Could not do tight layout.\n"
            plt.savefig(outName,dpi=72)

    else:
        try:
            plt.tight_layout()
        except ValueError:
            LOG += "Could not do tight layout.\n"
        plt.savefig(outName,dpi=72)

    LOG += "peaks: %s, valleys: %s, nr: %s\n" % (peaks, valleys, nr)
    plt.close('all')

    return LOG, stats


def main():
    opts = parseArgs()
    if not Path(opts.output_dir).is_dir():
        Path(opts.output_dir).mkdir()
    for input_file in Path(opts.input_dir).iterdir():
        # skip hidden files
        if str(input_file.name).startswith("."):
            continue
        print("Poly-plotting " + str(input_file))
        LOG, stats = plotPolymorphism(input_file, opts.output_dir, opts.window_boundary_size, opts.minimum_window_size)
        output_file = Path(opts.output_dir) / Path(input_file).stem
        with open(str(output_file), "w") as outfh:
            outfh.write(LOG)
            outfh.write(str(stats))

if __name__ == '__main__':
    main()
