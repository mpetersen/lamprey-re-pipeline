#!/usr/bin/env python
# this script reads in alignments and computes polymorphism scores, in parallel

from os import listdir
from os.path import isfile, join
import os
import sys
from pathlib import Path
import multiprocessing as mp
import plot_polymorphism
from argparse import ArgumentParser
import re

if "snakemake" in locals():
    alndir = Path(str(snakemake.input))
    window = snakemake.params.min_win_size
    statsfile = Path(str(snakemake.output.stats))
    outdir = Path(str(snakemake.output.plotsdir))
    num_threads = snakemake.threads
else:
    parser = ArgumentParser(description = "This runs several polymorphism calculation processes in parallel.")
    parser.add_argument("-a", "--alignment-dir", dest = "alndir", required = True, help = "Cluster alignment dir")
    parser.add_argument("-w", "--window-size", dest = "window", required = False, type = float, default = 0.3, help = "Scoring window size as a fraction of the alignment length. Default: 0.3")
    parser.add_argument("-s", "--stats-file", dest = "statsfile", required = True, type = str, help = "Stats file")
    parser.add_argument("-o", "--output-dir", dest = "outdir", required = True, type = str, help = "Output dir; where the plots are placed")
    parser.add_argument("-t", "--threads", dest = "num_threads", required = False, default = 1, type = int, help = "Number of parallel threads (processes)")
    opts = parser.parse_args()
    alndir = Path(opts.alndir)
    window = opts.window
    statsfile = Path(opts.statsfile)
    outdir = Path(opts.outdir)
    num_threads = opts.num_threads

print("Collecting alignment files...")
onlyfiles = [ Path(f) for f in listdir(alndir) if isfile(join(alndir, f)) ]
alnfiles = [ Path(alndir) / Path(f) for f in onlyfiles if not str(f.name).startswith(".") ] # filter out hidden files, such as snakemake timestamps

def plot_poly(infile, outdir, window_size):
    LOG, stats = plot_polymorphism.plotPolymorphism(infile, outdir, minimum_window_size = window_size)
    return ( LOG, stats )

# make output dir
if not outdir.is_dir():
    print("Creating output dir " + str(outdir))
    outdir.mkdir(exist_ok = True)

# run dat shit in parallel. prepare arguments
arguments = [ [ str(file), outdir, window ] for file in alnfiles ]

# that's cool.
print("Computing polymorphism scores")
pool = mp.Pool(num_threads)
results = pool.starmap(plot_poly, arguments)

# write results table
with open(str(statsfile), "w") as outfh:
    outfh.write("name\tfailed filter\talignment length\tsequences\tpeaks\tvalleys\tscoring window start\tscoring window end\tscoring window length\tposition scores\n")
    for r in [ res[1] for res in results ]:
        r["name"] = re.sub("\.aln$", "", r["name"])
        if r["failed filter"] == None or r["failed filter"] >= 1:
            outfh.write("{name}\t{failed}\t{length}\t{seqs}\t{peaks}\t{valleys}\t{window_start}\t{window_end}\t{window_length}\t{scores}\n".format(
                name = r["name"],
                failed = str(r["failed filter"]),
                length = str(r["alignment length"]),
                seqs = str(r["sequences"]),
                peaks = str(r["peaks"]) if "peaks" in r else "",
                valleys = str(r["valleys"]) if "valleys" in r else "",
                window_start = str(r["scoring window start"]),
                window_end = str(r["scoring window end"]),
                window_length = str(r["scoring window end"] - r["scoring window start"]),
                scores = ",".join(map(str, r["position scores"])) if "position scores" in r else ""
                )
            )
        else:
            outfh.write("{name}\t{failed}\t{length}\t{seqs}\t{peaks}\t{valleys}\t{window_start}\t{window_end}\t{window_length}\t{scores}\n".format(
                name = r["name"],
                failed = str(r["failed filter"]),
                length = str(r["alignment length"]),
                seqs = str(r["sequences"]),
                peaks = "",
                valleys = "",
                window_start = "",
                window_end = "",
                window_length = "",
                scores = ""))
    outfh.close()
