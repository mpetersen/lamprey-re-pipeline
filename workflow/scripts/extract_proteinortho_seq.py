#!/usr/bin/python
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
import sys
import os
import threading
import subprocess
from argparse import ArgumentParser

#clustalw = "/package/clustalw-2.1/clustalw2"
clustalw = "clustalo"

def parseArgs():
    parser = ArgumentParser(description="This script extracts the sequences of the proteins that were grouped together by Proteinortho and runs a multiple sequence alignment on each group.")
    parser.add_argument("-f", "--fasta", dest="fasta", required=True, help="comma-separated list of protein fasta files")
    parser.add_argument("-p", "--proteinortho", dest="proteinortho", required=True, help="Proteinotho results")
    parser.add_argument("-o", "--output-dir", dest="outDir", required=True, help="directory for the results")
    #parser.add_argument("-t", "--tsv", dest="tsv", required=False, action = "store_true", default=False, help="column delimiter in your sra_results.csv is tab (default: comma)")
    options = parser.parse_args()

    options.fastas = options.fasta.split(",")
    for f in options.fastas:
        if (not os.access(f, os.R_OK)):
            print("Error: protein fasta file \"%s\" does not exist or is not readable." % (f), file=sys.stderr)
            exit(-1)
    if (not os.access(options.proteinortho, os.R_OK)):
        print("Error: Proteinotho results file does not exist or is not readable.", file=sys.stderr)
        exit(-1)
    if (not os.path.isdir(options.outDir)):
        print("Error: The specified output directory does not exist or is not a directory.", file=sys.stderr)
        exit(1)
    options.outDir = os.path.abspath(options.outDir)
    options.proteinortho = os.path.abspath(options.proteinortho)

    return options


def readFasta (fastas):
    sequences = {}
    for fastaFile in fastas:
        input_handle = open(fastaFile, "r")
        for record in SeqIO.parse(input_handle, "fasta"):
            sequences[record.id] = record
        input_handle.close()
    return sequences

def getSeqs(line):
    seqIds = []
    columns = line.split("\t")
    for c in columns[3:]:
        c = c.strip()
        if (c != "*") and (c != ""):
            if (c.find(",") != -1):
                for x in c.split(","):
                    seqIds.append(x)
            else:
                seqIds.append(c)
    print(line)
    print(seqIds)
    return seqIds

def writeFasta(seqIds, sequences, filename):
    out_handle = open(filename, "w")
    for s in seqIds:
        if (s in sequences):
            SeqIO.write(sequences[s], out_handle, "fasta")
        else:
            print("Error: %s not found in provided fasta files." % (s), file=sys.stderr)
    out_handle.close()

def align(line, clusterNo, sequences):
    if (not line.startswith("#")):
        filename = "fasta/cluster%.6i.fa" % (clusterNo)
        outFile = "cluster%.6i.aln" % (clusterNo)
        seqIds = getSeqs(line.strip())
        writeFasta(seqIds, sequences, filename)
        #cmd = "%s --infile %s --seqtype PROTEIN --outfmt fasta --outfile %s" % (clustalw, filename, outFile)
        #subprocess.check_call(cmd, shell=True)
    return

class alignThread(threading.Thread):
    def __init__(self, lines, startClusterNo, sequences):
        self.lines = lines
        self.sequences = sequences
        self.startClusterNo = startClusterNo
        threading.Thread.__init__(self)
    def run(self):
        clusterNo = self.startClusterNo
        for line in self.lines:
            align(line, clusterNo, self.sequences)
            clusterNo += 1



def main():
    options = parseArgs()
    sequences = readFasta(options.fastas)
    os.chdir(options.outDir)
    if (not os.path.isdir("fasta")):
        os.mkdir("fasta")
    orthoInput = open(options.proteinortho, "r")
    i = 0
    threads = []
    lines = []

    for line in orthoInput:
        if (not line.startswith("#")):
            lines.append(line)
            i += 1
        if len(lines) == 10000:
            thread = alignThread(lines, i-10000, sequences)
            thread.start()
            threads.append(thread)
            lines = []

    # don't forget left over lines
    thread = alignThread(lines, i-len(lines), sequences)
    thread.start()
    threads.append(thread)

    for t in threads:
        t.join()
    orthoInput.close()


if __name__ == '__main__':
    main()
