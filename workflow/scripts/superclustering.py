#!/usr/bin/env python

import sys
import csv
import json
import re

if "snakemake" in locals():
    table_file  = str(snakemake.input)
    output_file_json = str(snakemake.output.json)
    output_file_tsv = str(snakemake.output.tsv)
else:
    table_file  = sys.argv[1]
    output_file_json = sys.argv[2]
    output_file_tsv = sys.argv[3]

clusters = { }

with open(table_file, "r") as f:
    reader = csv.DictReader(f, delimiter = "\t")
    na_clusters = [ ]
    for row in reader:
        row["cluster_id"] = re.sub("_.+", "", row["SeqName"]) # add stripped cluster id
        if row["Description"] == "---NA---":
            # Check whether we saw this NA cluster before
            if row["SeqName"] in na_clusters:
                print("Skipping SeqName '" + row["SeqName"] + "' because it already exists in supercluster for description: " + row["Description"])
                continue
            # otherwise, add number and note we've seen it
            row["Description"] = "NA_" + row["rowid"]
            na_clusters.append(row["SeqName"]) # not sure why
        if not row["Description"] in clusters: # first time we see this description
            row["index"] = 1
            clusters[row["Description"]] = [ row ]
        else: # seen this description before, add
            row["index"] = clusters[row["Description"]][-1]["index"] + 1 # bump index
            clusters[row["Description"]].append(row)

columns = ["Supercluster_nr", "Description", "index_of", "num_clusters", "cluster_id", "SeqName"]

with open(output_file_json, "w") as out_json, open(output_file_tsv, "w") as out_tsv:
    out_json.write(json.dumps(clusters, indent = 2)) # just dump the whole thing as JSON
    out_tsv.write("\t".join(columns) + "\n")
    c = 0
    for descr in clusters:
        c = c + 1
        i = 0
        n = len(clusters[descr])
        for clust in clusters[descr]:
            i = i + 1
            line = "\t".join([ str(c).zfill(3), clust["Description"], str(i), str(n), clust["cluster_id"], clust["SeqName"] ])
            out_tsv.write(line + "\n")
    out_json.close()
    out_tsv.close()
