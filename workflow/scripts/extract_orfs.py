#!/usr/bin/python
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
import sys


suffix = "_clstr.fa.transdecoder.bed"

def readOrfs (sample):
    inFile = "%s%s" % (sample, suffix)
    input_handle = open(inFile, "r")
    orfs = {}
    input_handle.readline()
    for record in input_handle:
        columns = record.split("\t")
        orfs[columns[0]] = columns[1:3]
    input_handle.close()
    return orfs
    
def main():
    sample = sys.argv[1]
    orfs = readOrfs(sample)
    for record in SeqIO.parse(sys.stdin, "fasta"):
        if record.id in orfs:
            SeqIO.write(record, sys.stdout, "fasta")


if __name__ == '__main__':
    main()
