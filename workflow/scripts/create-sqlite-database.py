#!/usr/bin/env python3
import sqlite3
import sys

if 'snakemake' in locals():
    db_file     = snakemake.output.db
    ok_file  = snakemake.output.ok
else:
    db_file  = sys.argv[1]
    ok_file  = sys.argv[2]

sql_queries = [ ]
# first drop all tables
sql_queries.append("DROP TABLE IF EXISTS go")
sql_queries.append("DROP TABLE IF EXISTS filters")
sql_queries.append("DROP TABLE IF EXISTS filter_flags")
sql_queries.append("DROP TABLE IF EXISTS filter_keystrings")

sql_queries.append("""
    CREATE TABLE IF NOT EXISTS go (
        rowid INTEGER PRIMARY KEY AUTOINCREMENT,
        True TEXT,
        Tags TEXT,
        Description TEXT,
        Length INTEGER NOT NULL,
        SeqName TEXT NOT NULL,
        Hits INTEGER NOT NULL,
        e_Value REAL NOT NULL,
        sim_mean REAL NOT NULL,
        Number_GO_IDs INTEGER NOT NULL,
        GO_IDs TEXT,
        GO_Names TEXT,
        Enzyme_Codes TEXT,
        Enzyme_Names TEXT,
        InterPro_IDs TEXT,
        InterPro_GO_IDs TEXT,
        InterPro_GO_Names TEXT,
        filter_flags INTEGER,
        f_blast_hit INTEGER,
        f_ipr_hit INTEGER,
        f_matches_keystring INTEGER,
        f_uncharacterized INTEGER,
        f_plasma_membrane INTEGER,
        f_transmembrane INTEGER,
        f_transporter_protein INTEGER,
        f_keystrings INTEGER
    );
    """)

sql_queries.append("""
    CREATE TABLE IF NOT EXISTS filters (
        rowid INTEGER PRIMARY KEY AUTOINCREMENT,
        flag INTEGER NOT NULL,
        description TEXT NOT NULL
    );
    """)

sql_queries.append("""
    CREATE TABLE IF NOT EXISTS filter_flags (
        rowid INTEGER PRIMARY KEY AUTOINCREMENT,
        flag INTEGER NOT NULL,
        filter_keystrings_remove INTEGER NOT NULL DEFAULT 0,
        filter_unchar_without_tm_remove INTEGER NOT NULL DEFAULT 0,
        filter_unchar_with_tm_keep INTEGER NOT NULL DEFAULT 0,
        filter_transporter_remove INTEGER NOT NULL DEFAULT 0,
        filter_cell_membrane_keep INTEGER NOT NULL DEFAULT 0
    );
    """)

sql_queries.append("""
    CREATE TABLE IF NOT EXISTS filter_keystrings (
        rowid INTEGER PRIMARY KEY AUTOINCREMENT,
        keystring TEXT NOT NULL
    );
    """)

sql_queries.append("""
    INSERT INTO filters
        (flag, description)
    VALUES
        (1, "keystrings: remove"),
        (2, "uncharacterized without TM domain: remove"),
        (4, "uncharacterized with TM domain: keep"),
        (8, "transporter protein: remove"),
        (16, "plasma membrane localisation: keep")
    ;
    """)

sql_queries.append("""
    INSERT INTO filter_flags
        (flag, filter_keystrings_remove, filter_unchar_without_tm_remove, filter_unchar_with_tm_keep, filter_transporter_remove, filter_cell_membrane_keep)
    VALUES
        (0, 0, 0, 0, 0, 0),
        (1, 1, 0, 0, 0, 0),
        (2, 0, 1, 0, 0, 0),
        (3, 1, 1, 0, 0, 0),
        (4, 0, 0, 1, 0, 0),
        (5, 1, 0, 1, 0, 0),
        (6, 0, 1, 1, 0, 0),
        (7, 1, 1, 1, 0, 0),
        (8, 0, 0, 0, 1, 0),
        (9, 1, 0, 0, 1, 0),
        (10, 0, 1, 0, 1, 0),
        (11, 1, 1, 0, 1, 0),
        (12, 0, 0, 1, 1, 0),
        (13, 1, 0, 1, 1, 0),
        (14, 0, 1, 1, 1, 0),
        (15, 1, 1, 1, 1, 0),
        (16, 0, 0, 0, 0, 1),
        (17, 1, 0, 0, 0, 1),
        (18, 0, 1, 0, 0, 1),
        (19, 1, 1, 0, 0, 1),
        (20, 0, 0, 1, 0, 1),
        (21, 1, 0, 1, 0, 1),
        (22, 0, 1, 1, 0, 1),
        (23, 1, 1, 1, 0, 1),
        (24, 0, 0, 0, 1, 1),
        (25, 1, 0, 0, 1, 1),
        (26, 0, 1, 0, 1, 1),
        (27, 1, 1, 0, 1, 1),
        (28, 0, 0, 1, 1, 1),
        (29, 1, 0, 1, 1, 1),
        (30, 0, 1, 1, 1, 1),
        (31, 1, 1, 1, 1, 1)
    ;
    """)

# open connection and execute all queries
conn = sqlite3.connect(str(db_file))
c = conn.cursor() # Cursor object executes queries
for query in sql_queries:
    c.execute(query)
conn.commit()
conn.close()

okf = open(str(ok_file), "w")
okf.close()

