#!/usr/bin/python
# this script reads in ClustalW alignments and computes a polymorphism score

import sys
import os
import os.path
import multiprocessing
#import threading
#import subprocess
sys.path.append("/data/projects_3/diehl/holland/scripts")
import plot_polymorphism
from argparse import ArgumentParser

NUMBER_OF_PROCESSES = 12

def parseArgs():
    parser = ArgumentParser(description="This scripts reads in directories of ClustalW alignments and plots the polymorphism scores")
    parser.add_argument("directories", nargs="+", help="input directories")
    parser.add_argument("-o", "--output-dir", dest="outDir", required=True, help="directory for the log files and plots")
    parser.add_argument("-s", "--stats-file", dest="statsFile", help="file for summary statistics")
    parser.add_argument("-p", "--processes", type=int, dest="p", required=True, help="number of parallel processes to start")
    options = parser.parse_args()
    
    for f in options.directories:
        if (not os.path.isdir(f)):
            print("Error: input directory \"%s\" does not exist or is not readable." % (f), file=sys.stderr)
            exit(-1)
    if (not os.path.isdir(options.outDir)):
        print("Error: The specified output directory does not exist or is not a directory.", file=sys.stderr)
        exit(1)
    options.outDir = os.path.abspath(options.outDir)
    options.directories = list(map(os.path.abspath,options.directories))
    
    return options

def worker(input, output):
    for alignment,outdir in iter(input.get, 'STOP'):
        LOG, stats = plot_polymorphism.plotPolymorphism( alignment,outdir )
        print(LOG)
        output.put(stats)

def main():
    options = parseArgs()
    
    if (options.p):
        NUMBER_OF_PROCESSES = int(options.p)
        
    tasks = 0
    # Create queues
    task_queue = multiprocessing.Queue()
    done_queue = multiprocessing.Queue()
    
    # Submit tasks
    for directory in options.directories:
        alignments = os.listdir(directory)
        alignments.sort()
        for alignment in alignments:
            if (alignment.endswith(".aln.fa")):
                t = ("%s/%s" % (directory,alignment), options.outDir)
                task_queue.put(t)
                tasks += 1
    
    
    #Start worker processes
    for _i in range(NUMBER_OF_PROCESSES):
        multiprocessing.Process(target=worker, args=(task_queue, done_queue)).start()

    # Get results
    results = []
    passed = 0
    failed = 0
    for _i in range(tasks):
        stats = done_queue.get()
        if (stats["passed filters"]):
            results.append(stats)
            passed += 1
        else:
            failed += 1
    print("%i passed the filters, %i failed" % (passed, failed))
    
    # Tell child processes to stop
    for _i in range(NUMBER_OF_PROCESSES):
        task_queue.put('STOP')
    
#    stats = {   "name": name,
#                "alignment length": alnLength,
#                "sequences": alnDepth,
#                "peaks": peaks,
#                "valleys": valleys,
#                "scoring window": nr,
#                "passed filters": passFilters,
#                "position scores": allColScores,
#                "window scores": windowScores
#                }
    
    if options.statsFile:
        out = open(options.statsFile,'w')
        out.write("name\talignment length\tsequences\tpeaks\tvalleys\tscoring window\tposition scores\n")
        for r in results:
            out.write("%s\t%i\t%i\t%i\t%i\t%i\t%s\n" % (r["name"], r["alignment length"], r["sequences"], r["peaks"], r["valleys"], r["scoring window start"], r["scoring window end"], ",".join(r["position scores"])))
        out.close()

if __name__ == '__main__':
    main()
