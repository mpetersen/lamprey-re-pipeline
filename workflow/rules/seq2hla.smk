rule seq2hla:
    input:
        "data/raw/{sample}.fastq.gz"
    output:
    conda:
        "../envs/seq2hla.yaml"
    params:
        trim: 5
    shell:
        "seq2HLA -1 {input.r1} -2 {input.r2} -r 'results/130_seq2HLA/{wildcards.sample}' -p {threads} -3 {params.trim}"
