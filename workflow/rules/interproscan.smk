#########################################################################
# Everything below is InterProScan, which is not done in this workflow. #
# Please ignore.                                                        #
#########################################################################

localrules: download_iprscan, download_panther, install_iprscan
rule download_iprscan:
    output:
        tarball = "interproscan-{iprscan_version}-64-bit.tar.gz".format(iprscan_version = config["iprscan_version"]),
        md5sum_file = "interproscan-{iprscan_version}-64-bit.tar.gz.md5".format(iprscan_version = config["iprscan_version"]),
        ok_file = "interproscan-{iprscan_version}-64-bit.tar.gz.ok".format(iprscan_version = config["iprscan_version"])
    params:
        iprscan_version = config["iprscan_version"]
    shell:
        """
        wget --continue ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/{params.iprscan_version}/interproscan-{params.iprscan_version}-64-bit.tar.gz
        wget --continue ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/{params.iprscan_version}/interproscan-{params.iprscan_version}-64-bit.tar.gz.md5
        md5sum -c {output.md5sum_file} && touch {output.ok_file}
        """

rule download_panther:
    output:
        tarball = "panther-data-{panther_version}.tar.gz".format(panther_version = config["panther_version"]),
        md5sum_file = "panther-data-{panther_version}.tar.gz.md5".format(panther_version = config["panther_version"]),
        ok_file = "panther-data-{panther_version}.tar.gz.md5.ok".format(panther_version = config["panther_version"])
    params:
        panther_version = config["panther_version"]
    shell:
        """
        wget --continue ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/data/panther-data-{params.panther_version}.tar.gz
        wget --continue ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/data/panther-data-{params.panther_version}.tar.gz.md5
        md5sum -c {output.md5sum_file} && touch {output.ok_file}
        """

rule install_iprscan:
    input:
        iprscan_tarball = "interproscan-{iprscan_version}-64-bit.tar.gz".format(iprscan_version = config["iprscan_version"]),
        panther_tarball = "panther-data-{panther_version}.tar.gz".format(panther_version = config["panther_version"])
    output:
        "code/interproscan-{iprscan_version}/interproscan.sh"
    params:
        iprscan_version = config["iprscan_version"]
    shell:
        """
        tar -C code -xzf {input.iprscan_tarball}
        tar -C code/interproscan-{params.iprscan_version}/data -xzf {input.panther_tarball}
        """

# This is necessary because InterProScan doesn't like stop codons
localrules: remove_stop_codons
rule remove_stop_codons:
    input:
        "results/090_orfs_collapsed/{sample}_transdec_p_clstr_long.fa"
    output:
        "results/091_remove_stop_codons/{sample}_transdec.faa"
    shell:
        "sed 's/*//' {input} > {output}"

rule interproscan:
    input:
        iprscan = "code/interproscan-5.45-80.0/interproscan.sh",
        fasta = "results/091_remove_stop_codons/{sample}_transdec.faa"
    output:
        "results/100_interproscan/{sample}_transdec.tsv"
    log:
        "logs/interproscan.{sample}.log"
    conda:
        "../envs/java-11.yaml"
    threads:
        24
    params:
        basename = "results/100_interproscan/{sample}_transdec",
        analyses = "--applications Panther-8.1,PrositeProfiles-20.97,PfamA-27.0,PrositePatterns-20.97,PRINTS-42.0,SignalP-EUK-4.0,TMHMM-2.0c,SuperFamily-1.75", # these are the old versions that are no longer available!
        other = "--goterms --seqtype p --pathways"
    message:
        "Sample {wildcards.sample}: Running InterProScan"
    shell:
        """
        tempdir=$(mktemp -d /scratch/local/iprscan.XXXXXXXX)
        echo "## Using tempdir $tempdir"
        bash {input.iprscan} --cpu {threads} --tempdir $tempdir {params.other} --input {input.fasta} --output-file-base {params.basename} > {log}
        rm -r $tempdir
        """

## InterProScan needs a large download; I chose the most recent version as of 2020-06-23; downloaded from
## tarball: ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/5.45-80.0/interproscan-5.45-80.0-64-bit.tar.gz
## md5sum:  ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/5.45-80.0/interproscan-5.45-80.0-64-bit.tar.gz.md5
##
## The Panther database is even larger. I need to use a version that matches
## the InterProScan version (5.45-80.0 as of 2020-06-23). I am using the most
## recent version of both. The Panther db was downloaded from
## tarball: ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/data/panther-data-14.1.tar.gz
## md5sum: ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/data/panther-data-14.1.tar.gz.md5

