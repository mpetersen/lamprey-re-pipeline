def get_longest_representative(wildcards):
    clusters = glob_wildcards("results/130_passed-poly-filter/fasta/{cluster}.fa").cluster
    return map(lambda s: "results/140_mhc-search/fasta/" + str(s) + ".fa", clusters)

localrules: extract_longest_seq
rule extract_longest_seq:
    input: "results/130_passed-poly-filter/fasta/{cluster}.fa"
    output: "results/140_mhc-search/fasta/{cluster}.fa"
    conda: "../envs/biopython.yaml"
    script: "../scripts/extract-longest-seq.py"

localrules: find_mhc
rule find_mhc:
    input:
        get_longest_representative
    output:
        representatives_fasta = "results/140_mhc-search/all-cluster-representatives.fa",
        blastp = "results/140_mhc-search/blastp.out"
    envmodules:
        "blast"
    threads:
        12
    params:
        db = config["mhc_db"]
    shell:
        """
        cat {input} > results/140_mhc-search/all-cluster-representatives.fa
        blastp -num_threads {threads} -db {params.db} -query {output.representatives_fasta} -outfmt 7 -out {output.blastp}
        """

localrules: best_hits_per_cluster
rule best_hits_per_cluster:
    input:
        "results/140_mhc-search/blastp.out"
    output:
        "results/140_mhc-search/best-hits-per-cluster.tsv"
    script:
        "../scripts/make-mhc-hit-table.py"

rule search_in_ncbi:
    input:
        representatives_fasta = "results/140/all-cluster-representatives.fa"
    output:
        tsv = "results/150_blast-ncbi/blastp.out",
        xml = "results/150_blast-ncbi/blastp.xml"
    envmodules:
        "diamond"
    params:
        db = config["ncbi_database"],
        outfmt_tsv = "--outfmt 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore staxids sscinames stitle",
        outfmt_xml = "--outfmt 5"
    threads:
        24
    shell:
        """
        diamond blastp --threads {threads} --db {params.db} --query {input.representatives_fasta} {params.outfmt_tsv} --out {output.tsv}
        diamond blastp --threads {threads} --db {params.db} --query {input.representatives_fasta} {params.outfmt_xml} --out {output.xml}
        """

rule annotate_re8_re13:
    input:
        representatives_fasta = "results/151_interproscan-30/all-passing-longest.fa",
        re8_re13_fasta = config["re8_re13_fasta"]
    output:
        xml = "results/161_find-re8+re13-30/find-re8+re13.diamond.xml"
    envmodules:
        "diamond"
    threads:
        12
    shell:
        """
        mkdir -p $(dirname {output.xml})
        db="$(dirname {output.xml})/$(basename {input.representatives_fasta}).db"
        cat {input.representatives_fasta} > $db
        diamond makedb --in {input.representatives_fasta} --db $db
        diamond blastp --threads {threads} --db $db --query {input.re8_re13_fasta} --outfmt 5 --out {output.xml}
        """

localrules: convert_diamond_output
rule convert_diamond_output:
    input:
        "results/161_find-re8+re13-30/find-re8+re13.diamond.tsv"
