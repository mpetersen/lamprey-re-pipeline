localrules: plot_poly
rule plot_poly:
    input:
        "results/111_extract_proteinortho_clusters/aligned"
    output:
        plotsdir = directory("results/120_plot-polymorphism-30/plots"),
        stats = "results/120_plot-polymorphism-30/stats.txt"
    conda:
        "../envs/biopython.yaml"
    resources:
        mem = 100
    threads:
        48
    params:
        min_win_size = 0.3
    script:
        "../scripts/plot-poly-parallel.py"

localrules: extract_passing_files
rule extract_passing_files:
    input: "results/120_plot-polymorphism-30/stats.txt"
    output:
        passed_list = "results/131_passed-poly-filter-30/passed.txt",
        fasta_dir = directory("results/131_passed-poly-filter-30/fasta")
    script:
        "../scripts/extract-passing-clusters.py"

# this rule removes clusters with less than five individuals. It also picks out
# the longest sequence (not counting gaps) and writes it to a new file, thereby
# creating a representative sequence
localrules: remove_clusters_with_less_than_5_individuals
rule remove_clusters_with_less_than_5_individuals:
    input:
        passed_list = "results/131_passed-poly-filter-30/passed.txt",
        fasta_dir = "results/131_passed-poly-filter-30/fasta"
    output:
        species_per_cluster_table = "results/140_clusters-with-5-or-more-individuals/species-per-cluster.txt",
        fasta_dir = directory("results/140_clusters-with-5-or-more-individuals/fasta")
    conda:
        "../envs/biopython.yaml"
    script:
        "../scripts/extract-clusters-with-5-or-more-individuals.py"
