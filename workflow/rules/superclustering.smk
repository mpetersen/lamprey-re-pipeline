localrules: supercluster
rule supercluster:
    input:
        "results/210_manually-filtered-go-annotation/go-annotations.filtered.filtered.tsv"
    output:
        json = "results/220_superclustered/superclusters.json",
        tsv = "results/220_superclustered/superclusters.tsv"
    script:
        "../scripts/superclustering.py"

rule copy_fasta_and_plots:
    input:
        "results/210_manually-filtered-go-annotation/go-annotations.filtered.filtered.tsv"
    output:
        "results/230_filtered-clusters/passing-filters.txt"
    shell:
        """
        cut -f6 {input} \
        | grep -v 'SeqName' \
        | sed -e 's/_.\+//' \
        | sort \
        | uniq \
        > {output}

        mkdir -p $(dirname {output})/{{fasta_aligned,fasta_unaligned,plots}}

        cat {output} | while read cluster; do
            cp results/122_plot-polymorphism-30/plots/${{cluster}}.aln_prop.png $(dirname {output})/plots
            cp results/111_extract_proteinortho_clusters/unaligned/${{cluster}}.fa $(dirname {output})/fasta_unaligned
            cp results/111_extract_proteinortho_clusters/aligned/${{cluster}}.aln.fa $(dirname {output})/fasta_aligned
        done
        """

localrules: realign_superclusters
rule realign_superclusters:
    input:
        tsv = "results/220_superclustered/superclusters.tsv",
        cluster_dir = "results/111_extract_proteinortho_clusters/unaligned"
    output:
        unaligned = directory("results/221_superclusters-aligned/unaligned"),
        aligned = directory("results/221_superclusters-aligned/aligned")
    conda:
        "../envs/proteinortho.yaml"
    shell:
        """
        rm -rf {output.unaligned} {output.aligned}
        mkdir -p {output.unaligned} {output.aligned}
        cut -f1,5 {input.tsv} \
        | tail -n +2 \
        | while read sc clust; do \
            cat {input.cluster_dir}/${{clust}}.fa >> {output.unaligned}/supercluster_${{sc}}.fa
        done
        for file in {output.unaligned}/*.fa; do
            echo "Aligning $file "
            clustalo --infile $file  --seqtype PROTEIN --outfmt fasta --outfile {output.aligned}/$(basename $file  .fa).aln.fa
        done
        """

localrules: plot_poly_again
rule plot_poly_again:
    input:
        "results/221_superclusters-aligned/aligned"
    output:
        plotsdir = directory("results/222_plot-polymorphism-for-superclusters/plots"),
        stats = "results/222_plot-polymorphism-for-superclusters/stats.txt"
    conda:
        "../envs/biopython.yaml"
    resources:
        mem = 100
    threads:
        48
    params:
        min_win_size = 0.3
    script:
        "../scripts/plot-poly-parallel.py"
