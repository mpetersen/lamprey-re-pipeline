import pandas as pd
import datetime
import os

#now = datetime.datetime.now().isoformat(sep = "_", timespec = "minutes")

configfile: "config/config.yaml"

units = pd.read_csv(config["units"], sep = "\t", dtype = str, comment = "#").set_index("sample", drop = False)

def get_fastqs(wildcards):
    """Get raw FASTQ files from unit sheet."""
    files = units.loc[ wildcards.sample, [ "R1", "R2" ] ]
    return files.tolist()

def get_fastqs_for_indiv(wildcards):
    """Get raw FASTQ files from unit sheet."""
    files = units.loc[ wildcards.individual, [ "R1", "R2" ] ]
    return files.tolist()

localrules: link_fastqs
rule link_fastqs:
    input:
        get_fastqs
    output:
        "data/raw/{sample}_R1.fastq.gz",
        "data/raw/{sample}_R2.fastq.gz"
    shell:
        """
        ln -s {input[0]} {output[0]}
        ln -s {input[1]} {output[1]}
        """

rule pool_fastqs:
    input:
        get_fastqs_for_indiv
    output:
        "data/raw/{individual}_R1.fastq.gz",
        "data/raw/{individual}_R2.fastq.gz"
    shell:
        """
        cat {input[0]} > {output[0]}
        cat {input[1]} > {output[1]}
        """

localrules: xml2tsv
rule xml2tsv:
    input:
        "{name}.xml"
    output:
        "{name}.tsv"
    conda:
        "../envs/biopython.yaml"
    script:
        "../scripts/xml2tsv.py"
