rule proteinortho:
    input:
        lambda wildcards: expand("results/091_remove_stop_codons/{sample}_transdec.faa", sample = units["sample"])
    output:
        ortho_table = "results/110_proteinortho/samples.proteinortho.tsv",
        cluster_list = "results/110_proteinortho/samples.proteinortho.clusters.tsv"
    log:
        "logs/proteinortho.log"
    conda:
        "../envs/proteinortho.yaml"
    threads:
        12
    params:
        other = "--verbose"
    message:
        "Running ProteinOrtho with PoFF on {threads} threads"
    shell:
        """
        proteinortho --cpus={threads} --project=samples {params} {input} 2> {log}
        mv samples.proteinortho.* samples.proteinortho-graph samples.proteinortho-graph.summary samples.blast-graph samples.info $(dirname {output.ortho_table})
        awk 'NR == 0 { print } NR > 1 {{ printf("cluster%06d\t%s\n", NR - 2), $0 }}' {output.ortho_table} > {output.cluster_list}
        """

localrules: extract_orthologous_seqs
checkpoint extract_orthologous_seqs:
    input:
        proteinortho = "results/110_proteinortho/samples.proteinortho.tsv",
        fastas = expand("results/091_remove_stop_codons/{sample}_transdec.faa", sample = units["sample"])
    output:
        directory("results/111_extract_proteinortho_clusters/unaligned")
    log:
        "logs/extract_proteinortho_seqs.log"
    conda:
        "../envs/proteinortho.yaml"
    params:
        fasta_list = lambda wildcards, input: ",".join(input.fastas)
    shell:
        """
        mkdir -p {output}
        python workflow/scripts/extract_proteinortho_seq.py --fasta {params.fasta_list} --proteinortho {input.proteinortho} --output-dir $(dirname {output}) > {log}
        """

rule align_cluster:
    input:
        "results/111_extract_proteinortho_clusters/unaligned/{cluster}.fa"
    output:
        "results/111_extract_proteinortho_clusters/aligned/{cluster}.aln.fa"
    conda:
        "../envs/proteinortho.yaml"
    threads:
        1
    shell:
        """
        clustalo --infile {input} --seqtype PROTEIN --outfmt fasta --outfile {output}
        """
