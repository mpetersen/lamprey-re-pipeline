import sys

if not os.path.isfile("results/181_blast2go-30/all-passing-longest.blast2go.txt"):
    not_found_message = """File results/181_blast2go-30/all-passing-longest.blast2go.txt doesn't exist;
        you need to add it manually after running BLAST2GO.
        Import:
          - results/151_interproscan-30/all-passing-longest.iprscan.xml,
          - results/171_blast_candidates-against-nr-30/all-passing.blastp.sensitive.xml"""
    print(not_found_message)
    sys.exit(1)

# this rule creates the db AND imports the go annotation
localrules: import_go_annotation
rule import_go_annotation:
    input:
        db = "results/191_blast2go-filter-30/go-annotations.sqlite",
        go = "results/181_blast2go-30/all-passing-longest.blast2go.txt",
        filter_keystrings = config["filter_keystrings"]
    output:
        "results/191_blast2go-filter-30/import.ok"
    script:
        "../scripts/import-go-annotation.py"

localrules: filter_go_annotation
rule filter_go_annotation:
    input:
        db = "results/191_blast2go-filter-30/go-annotations.sqlite",
        import_ok = "results/191_blast2go-filter-30/import.ok"
    output:
        "results/191_blast2go-filter-30/go-annotations.filtered.tsv"
    run:
        import sqlite3
        import pandas as pd
        conn = sqlite3.connect(str(input.db))
        sql = """
        SELECT
            *
        FROM
            go
        WHERE
            ((f_blast_hit = 1 AND f_plasma_membrane = 1 OR f_transmembrane = 1) OR (f_blast_hit = 0 AND f_transmembrane = 1)) AND f_matches_keystring = 0 AND f_transporter_protein = 0
        ORDER BY go.rowid
        """
        db_df = pd.read_sql(sql, conn)
        db_df.to_csv(str(output), index = False, sep = "\t")

localrules: make_new_keyword_list
rule make_new_keyword_list:
    input:
        "results/191_blast2go-filter-30/go-annotations.filtered.tsv"
    output:
        "results/200_keywords-from-filtered-table/keywords-counts.txt"
    shell:
        """
        cut -f4 {input} \
        | tail -n +2 \
        | grep -v -e 'NA--' -e 'uncharacterized protein' \
        | sed -e 's/\([0-9]\+\)\?\( \|-\)[0-9]\+\([A-Z]\)\?$//' -e 's/ [A-Z]$//' \
        | sort \
        | uniq -c \
        | sort -rn \
        | awk 'BEGIN {{ OFS="\t"; print "count", "description" }} {{ sub("^ +", ""); sub(" ", "\t"); print }}' \
        > {output}
        """

if not os.path.isfile(config["additional_filter_keystrings"]):
    not_found_message = "File " + str(config["additional_filter_keystrings"]) + " doesn't exist; you need to add it after manually creating it."
    print(not_found_message)
    sys.exit(1)

localrules: filter_go_annotation_again
rule filter_go_annotation_again:
    input:
        table = "results/191_blast2go-filter-30/go-annotations.filtered.tsv",
        filter_strings = config["additional_filter_keystrings"]
    output:
        "results/210_manually-filtered-go-annotation/go-annotations.filtered.filtered.tsv"
    run:
        import csv
        import re
        keystrings = [ ]
        with open(str(input.filter_strings), "r") as f:
            keystrings = f.read().splitlines()
            f.close()
        with open(str(input.table), "r") as tf, open(str(output), "w") as out:
            tsv_in = csv.reader(tf, delimiter = "\t")
            tsv_out = csv.writer(out, delimiter = "\t")
            for row in tsv_in:
                if not any(keystring in row[3] for keystring in keystrings):
                    tsv_out.writerow(row)

