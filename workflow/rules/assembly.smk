rule trimmomatic_pe:
    input:
        R1 = "data/raw/{sample}_R1.fastq.gz",
        R2 = "data/raw/{sample}_R2.fastq.gz"
    output:
        R1 = temp("results/010_trimmomatic/{sample}_R1.fastq.gz"),
        R2 = temp("results/010_trimmomatic/{sample}_R2.fastq.gz"),
        R1_unpaired = temp("results/010_trimmomatic/{sample}_R1.unpaired.fastq.gz"),
        R2_unpaired = temp("results/010_trimmomatic/{sample}_R2.unpaired.fastq.gz")
    log:
        "logs/trimmomatic_pe.{sample}.summary"
    conda:
        "../envs/qc.yaml"
    threads:
        12
    params:
        truseq3_adapters_pe = config["illumina_pe_adapters_fasta"],
        trim_window = 2,
        trim_leading = 30,
        trim_trailing = 10,
    message:
        "Sample {wildcards.sample}: Trimmomatic with {threads} threads"
    shell:
        "trimmomatic PE -threads {threads} -summary {log} {input.R1} {input.R2} {output.R1} {output.R2} {output.R1_unpaired} {output.R2_unpaired} ILLUMINACLIP:{params.truseq3_adapters_pe}:{params.trim_window}:{params.trim_leading}:{params.trim_trailing}"

rule trimmomatic_se:
    input:
        "data/raw/{sample}.fastq.gz",
    output:
        temp("results/010_trimmomatic/{sample}.fastq.gz"),
    wildcard_constraints:
        sample = "^SRR"
    log:
        "logs/trimmomatic_se.{sample}.summary"
    benchmark:
        "benchmarks/trimmomatic_se.{sample}.benchmark.txt"
    conda:
        "../envs/qc.yaml"
    threads:
        12
    params:
        truseq3_adapters_se = config["illumina_pe_adapters_fasta"],
        trim_window = 2,
        trim_leading = 30,
        trim_trailing = 10,
    message:
        "Sample {wildcards.sample}: Trimmomatic with {threads} threads"
    shell:
        "trimmomatic SE -threads {threads} -summary {log} {input} {output} ILLUMINACLIP:{params.truseq3_adapters_se}:{params.trim_window}:{params.trim_leading}:{params.trim_trailing}"

## Quality filter according to khmer protocols; no need to interleave reads since they are single-end here
localrules: quality_filter
rule quality_filter:
    input:
        "results/010_trimmomatic/{sample}_R{read}.fastq.gz",
    output:
        temp("results/020_filtering/{sample}_R{read}.qc.fq.gz")
    log:
        "logs/quality_filter.{sample}_R{read}.log"
    benchmark:
        "benchmarks/quality_filter.{sample}_R{read}.benchmark.txt"
    conda:
        "../envs/qc.yaml"
    params:
        filt = "-v -Q33 -q 30 -p 50",
        trim = "-Q 33 -t 15"
    message:
        "Sample {wildcards.sample}: Quality filtering and trimming and replacing whitespaces"
    shell:
        """
        zcat {input} \
        | fastq_quality_filter {params.filt} \
        | fastq_quality_trimmer {params.trim} \
        | sed -e 's/ /_/' -e 's/$/\/{wildcards.read}/' \
        | gzip -1 \
        > {output} 2> {log}
        """

rule fastp_pe:
    input:
        sample = ["data/raw/{sample}_R1.fastq.gz", "data/raw/{sample}_R2.fastq.gz"]
    output:
        trimmed = ["results/021_fastp/{sample}_R1.fastq.gz", "results/021_fastp/{sample}_R2.fastq.gz"],
        html = "report/fastp.{sample}.html",
        json = "report/fastp.{sample}.json"
    log:
        "logs/fastp.{sample}.log"
    params:
        extra = ""
    threads: 4
    wrapper:
        "0.63.0/bio/fastp"


# Trinity stranded assembly

## digital normalisation is on by default since 2016, so included with Trinity. Good!

# replace spaces in read names because Trinity doesn't like them
localrules: rename_reads
rule rename_reads:
    input:
        "results/020_filtering/{sample}_R{read}.qc.fq.gz"
    output:
        temp("results/030_renamed/{sample}_R{read}.qc.fq.gz")
    message:
        "Sample {wildcards.sample}: Renaming reads to replace all spaces with underscores because Trinity doesn't like spaces"
    shell:
        "zcat {input} | sed -e 's/ /_/' -e 's/$/\/{wildcards.read}/' | gzip -1 > {output}"

# Assembly with Trinity
rule trinity:
    input:
        R1 = "data/raw/{sample}_R1.fastq.gz",
        R2 = "data/raw/{sample}_R2.fastq.gz"
    output:
        "results/040_assembly_trinity/{sample}_trinity/Trinity.fasta"
    log:
        "logs/trinity.{sample}.log"
    benchmark:
        "benchmarks/trinity.{sample}.benchmark.txt"
    envmodules:
        "Trinity"
    conda:
        "../envs/assembly.yaml"
    threads:
        4
    params:
        mem = "50G",
        outdir = "results/040_assembly_trinity/{sample}_trinity",
        other = "--trimmomatic"
    message:
        "Sample {wildcards.sample}: De novo assembly with Trinity on {threads} threads"
    shell:
        """
        Trinity --seqType fq --SS_lib_type RF --left {input.R1} --right {input.R2} --output {params.outdir} --CPU {threads} --bflyCPU {threads} --inchworm_cpu {threads} --max_memory {params.mem} {params.other} > {log}
        """

rule trinity_se:
    input:
        "results/030_renamed/{sample}.qc.fq.gz"
    output:
        "results/040_assembly_trinity/{sample}_trinity/Trinity.fasta"
    log:
        "logs/trinity_se.{sample}.log"
    benchmark:
        "benchmarks/trinity_se.{sample}.benchmark.txt"
    envmodules:
        "Trinity"
    conda:
        "../envs/assembly.yaml"
    threads:
        24
    params:
        mem = "50G",
        outdir = "results/040_assembly_trinity/{sample}_trinity",
        other = ""
    message:
        "Sample {wildcards.sample}: De novo assembly with Trinity on {threads} threads"
    shell:
        """
        Trinity --seqType fq --SS_lib_type R --single {input} --output {params.outdir} --CPU {threads} --bflyCPU {threads} --inchworm_cpu {threads} --max_memory {params.mem} {params.other} > {log}
        """

localrules: pack_assembly
rule pack_assembly:
    input:
        "results/040_assembly_trinity/{sample}_trinity/Trinity.fasta"
    output:
        "results/040_assembly_trinity/{sample}_trinity.tar.bz2"
    shell:
        """
        cd $(dirname {input})/..
        tar -cvf $(basename {output}) $(basename $(dirname {input}))
        """

localrules: clean_assembly
rule clean_assembly:
    input:
        fasta = "results/050_assembly_renamed/{sample}_Trinity.fa",
        tar = "results/040_assembly_trinity/{sample}_trinity.tar.bz2"
    shell:
        """
        empty=$(mktemp -d)
        rsync -au --delete $empty/ $(dirname {input.tar})/
        rmdir $(dirname {input.tar})
        rmdir $empty
        """

# Adding sample name to fasta ID
localrules: rename_seqs
rule rename_seqs:
    input:
        "results/040_assembly_trinity/{sample}_trinity/Trinity.fasta"
    output:
        "results/050_assembly_renamed/{sample}_Trinity.fa"
    message:
        "Sample {wildcards.sample}: Adding sample information to sequence headers"
    shell:
        "sed 's/^>/>{wildcards.sample}_/' {input} > {output}"

# Clustering with cd-hit
localrules: cluster_transcripts
rule cluster_transcripts:
    input:
        "results/050_assembly_renamed/{sample}_Trinity.fa"
    output:
        "results/060_clustering/{sample}_clstr.fa"
    log:
        "logs/cd-hit.transcripts.{sample}.log"
    envmodules:
        "cd-hit"
    conda:
        "../envs/transdecoder.yaml"
    threads:
        12
    params:
        "-c 0.98 -n 8 -d 0 -M 1000"
    message:
        "Sample {wildcards.sample}: Clustering assembled transcripts"
    shell:
        "cd-hit-est -T {threads} {params} -i {input} -o {output} > {log}"

# ORF filtering
rule predict_orf:
    input:
        "results/060_clustering/{sample}_clstr.fa"
    output:
        directory("results/070_orf-filtering/{sample}")
    log:
        "logs/predict_orf.{sample}.log"
    conda:
        "../envs/transdecoder.yaml"
    message:
        "Sample {wildcards.sample}: Finding ORFs"
    shell:
        """
        echo '## Running TransDecoder.LongOrfs' >&2
        TransDecoder.LongOrfs -t {input} -S -O {output} > {log}
        echo '## Running TransDecoder.Predict' >&2
        TransDecoder.Predict -t {input} -O {output} >> {log}
        # Stupid TransDecoder creates output in the current directory, move them
        mv {wildcards.sample}_clstr.fa.transdecoder.* {output}
        """

# Read the BED file to extract the nucleotide sequences (not sure how much
# sense this makes since TransDecoder outputs them as well...)
localrules: extract_nt_seqs
rule extract_nt_seqs:
    input:
        "results/060_clustering/{sample}_clstr.fa"
    output:
        "results/071_extracted_orfs/{sample}_clstr_orfs.fa"
    shell:
        "cat {input} | python workflow/scripts/extract_orfs.py {wildcards.sample} > {output}"

# reformat fasta naming of transdecoder proteins
localrules: reformat_transdecoder
rule reformat_transdecoder:
    input:
        "results/070_orf-filtering/{sample}"
    output:
        nt = "results/080_transdecoder_renamed/{sample}_transdec.fa",
        aa = "results/080_transdecoder_renamed/{sample}_transdec_p.fa",
        bed = "results/080_transdecoder_renamed/{sample}_orfs.bed"
    message:
        "Sample {wildcards.sample}: Reformatting TransDecoder sequence headers"
    shell:
        """
        echo '/^>/ {{ printf("%s %s %s %s %s\\n", $1, $3, $4, $5, $6) }}' > scratch/awk.cmd
        echo '! /^>/ {{ print }}' >> scratch/awk.cmd
        awk -f scratch/awk.cmd {input}/{wildcards.sample}_clstr.fa.transdecoder.cds > {output.nt}
        awk -f scratch/awk.cmd {input}/{wildcards.sample}_clstr.fa.transdecoder.pep > {output.aa}

        # bed file without track line
        grep -v "^track" {input}/{wildcards.sample}_clstr.fa.transdecoder.bed > {output.bed}
        """

localrules: cluster_orfs
rule cluster_orfs:
    input:
        "results/080_transdecoder_renamed/{sample}_transdec_p.fa"
    output:
        clusters = "results/090_orfs_collapsed/{sample}_transdec_p_clstr.fa",
        long_clusters = "results/090_orfs_collapsed/{sample}_transdec_p_clstr_long.fa"
    log:
        "logs/cd-hit.orfs.{sample}.log"
    conda:
        "../envs/transdecoder.yaml"
    threads:
        12
    message:
        "Sample {wildcards.sample}: Collapsing and filtering ORFs"
    shell:
        """
        cd-hit -T {threads} -i {input} -o {output.clusters} -c 0.98 -d 0 > {log}
        python workflow/scripts/filter_short.py {output.clusters} > {output.long_clusters}
        """
