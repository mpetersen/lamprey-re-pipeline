# Restriction element annotation pipeline

Adopted from a script collection by Sarah Müller, written in 2013 and 2014.
Reproduced in Snakemake by Malte Petersen <petersen@ie-freiburg.mpg.de>

To add samples:

1. Edit config/units.yaml to add the sample number and path to the (single-ended) fastq file.

2. Run workflow:

    snakemake [options]

# Things I had to change

- I am using current versions of all tools, including Proteinortho, ClustalO, CD-Hit, Trinity, etc. I really couldn't be bothered to find the versions used in 2013.
- Digital normalisation is now included in the Trinity workflow, hence it was dropped from the workflow.
- InterProScan: The current version 5.45 has support for the Panther DB version 14.
- Proteinortho version 6.0.18 doesn't have the option `-a` and I don't know what it is. The number of concurrent threads? The only possible option would be `-alpha`, but the previous value of 12 seems too large for it.
